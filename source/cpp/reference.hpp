/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_MICROCORE_HPP_

#include "../../dependencies/com-wui-framework-xcppcommons/source/cpp/reference.hpp"

#include <commdlg.h>  // NOLINT
#include <Shobjidl.h>

#include "interfacesMap.hpp"
#include "Com/Wui/Framework/Microcore/sourceFilesMap.hpp"

// global-cef-includes-start
#include <include/cef_app.h> // NOLINT
#include <include/cef_base.h> // NOLINT
#include <include/cef_stream.h> // NOLINT
#include <include/cef_browser.h> // NOLINT
#include <include/cef_client.h> // NOLINT
#include <include/cef_task.h> // NOLINT
#include <include/cef_render_handler.h> // NOLINT
#include <include/cef_image.h> // NOLINT
#include <include/cef_request.h> // NOLINT
#include <include/cef_response.h> // NOLINT
#include <include/cef_response_filter.h> // NOLINT
#include <include/cef_cookie.h> // NOLINT
#include <include/cef_scheme.h> // NOLINT
#include <include/cef_browser_process_handler.h> // NOLINT
#include <include/cef_command_line.h> // NOLINT
#include <include/cef_version.h> // NOLINT
#include <include/base/cef_scoped_ptr.h> // NOLINT
#include <include/base/cef_lock.h> // NOLINT
#include <include/base/cef_ref_counted.h> // NOLINT
#include <include/base/cef_thread_checker.h> // NOLINT
#include <include/base/cef_bind.h> // NOLINT
#include <include/base/cef_build.h> // NOLINT
#include <include/wrapper/cef_helpers.h> // NOLINT
#include <include/wrapper/cef_message_router.h> // NOLINT
#include <include/wrapper/cef_resource_manager.h> // NOLINT
#include <include/wrapper/cef_closure_task.h> // NOLINT
#include <include/internal/cef_types_wrappers.h> // NOLINT
// global-cef-includes-stop

// generated-code-start
#include "Com/Wui/Framework/Microcore/App/App.hpp"
#include "Com/Wui/Framework/Microcore/App/Client.hpp"
#include "Com/Wui/Framework/Microcore/Application.hpp"
#include "Com/Wui/Framework/Microcore/Bridges/DragAndDropBridge.Windows.hpp"
#include "Com/Wui/Framework/Microcore/Browser/DragAndDropManager.Windows.hpp"
#include "Com/Wui/Framework/Microcore/Browser/Window.Windows.hpp"
#include "Com/Wui/Framework/Microcore/Browser/WindowBase.hpp"
#include "Com/Wui/Framework/Microcore/Browser/WindowManager.hpp"
#include "Com/Wui/Framework/Microcore/Commons/Configuration.hpp"
#include "Com/Wui/Framework/Microcore/Commons/SharedLibraryManager.Windows.hpp"
#include "Com/Wui/Framework/Microcore/Enums/ProcessExitCodes.hpp"
#include "Com/Wui/Framework/Microcore/Exceptions/DragAndDropManagerExceptions.hpp"
#include "Com/Wui/Framework/Microcore/Exceptions/SharedLibraryManagerExceptions.hpp"
#include "Com/Wui/Framework/Microcore/Exceptions/WindowBaseExceptions.hpp"
#include "Com/Wui/Framework/Microcore/Exceptions/WindowManagerExceptions.hpp"
#include "Com/Wui/Framework/Microcore/Handlers/BrowserProcessHandler.hpp"
#include "Com/Wui/Framework/Microcore/Handlers/DisplayHandler.hpp"
#include "Com/Wui/Framework/Microcore/Handlers/DragHandler.hpp"
#include "Com/Wui/Framework/Microcore/Handlers/LifeSpanHandler.hpp"
#include "Com/Wui/Framework/Microcore/Handlers/LoadHandler.hpp"
#include "Com/Wui/Framework/Microcore/Handlers/RenderHandler.hpp"
#include "Com/Wui/Framework/Microcore/Helpers/ScopedGLContext.Windows.hpp"
#include "Com/Wui/Framework/Microcore/Helpers/Utils.hpp"
#include "Com/Wui/Framework/Microcore/Helpers/Utils.Windows.hpp"
#include "Com/Wui/Framework/Microcore/Interfaces/IOsrDelegate.hpp"
#include "Com/Wui/Framework/Microcore/Loader.hpp"
#include "Com/Wui/Framework/Microcore/Renderer/OsrRenderer.hpp"
#include "Com/Wui/Framework/Microcore/Renderer/OsrRendererMacros.hpp"
#include "Com/Wui/Framework/Microcore/Runners/Runner.Linux.hpp"
#include "Com/Wui/Framework/Microcore/Runners/Runner.Windows.hpp"
#include "Com/Wui/Framework/Microcore/Runners/RunnerBase.hpp"
#include "Com/Wui/Framework/Microcore/Structures/MouseInformation.hpp"
#include "Com/Wui/Framework/Microcore/Structures/ProgramArguments.hpp"
#include "Com/Wui/Framework/Microcore/Structures/RendererSettings.hpp"
#include "Com/Wui/Framework/Microcore/Structures/WindowSettings.hpp"
// generated-code-end

#endif  // COM_WUI_FRAMEWORK_MICROCORE_HPP_  NOLINT
