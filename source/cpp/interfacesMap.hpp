/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef COM_WUI_FRAMEWORK_MICROCORE_INTERFACESMAP_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_MICROCORE_INTERFACESMAP_HPP_

namespace Com {
    namespace Wui {
        namespace Framework {
            namespace Microcore {
                class Application;
                class Loader;
                namespace App {
                    class Application;
                    class Client;
                }
                namespace Bridges {
                    class DragAndDropBridge;
                }
                namespace Browser {
                    class DragAndDropManager;
                    class Window;
                    class WindowBase;
                    class WindowManager;
                }
                namespace Commons {
                    class Configuration;
                    class SharedLibraryManager;
                }
                namespace Enums {
                    enum ProcessExitCodes : int;
                }
                namespace Exceptions {
                    struct WindowManagerIsNotSet;
                    struct ProcedureNotExists;
                    struct FailedToInitialize;
                    struct WindowNotExists;
                }
                namespace Handlers {
                    class BrowserProcessHandler;
                    class DisplayHandler;
                    class DragHandler;
                    class LifeSpanHandler;
                    class LoadHandler;
                    class RenderHandler;
                }
                namespace Helpers {
                    class ScopedGLContext;
                }
                namespace Interfaces {
                    class IOsrDelegate;
                }
                namespace Renderer {
                    class OsrRenderer;
                }
                namespace Runners {
                    class Runner;
                    class Runner;
                    class RunnerBase;
                }
                namespace Structures {
                    struct MouseInformation;
                    class ProgramArguments;
                    struct RendererSettings;
                    struct WindowSettings;
                }
            }
        }
    }
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_INTERFACESMAP_HPP_  // NOLINT
