/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_BRIDGES_DRAGANDDROPBRIDGE_WINDOWS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_BRIDGES_DRAGANDDROPBRIDGE_WINDOWS_HPP_

#ifdef WIN_PLATFORM

#include <MSVC/Callbacks/Callbacks.hpp>
#include <MSVC/Structures/DragData.hpp>

#include "../Commons/SharedLibraryManager.Windows.hpp"

namespace Com::Wui::Framework::Microcore::Bridges {
    class DragAndDropBridge {
        using OnDrop = Com::Wui::Framework::Microcore::MSVC::Callbacks::OnDrop;
        using OnDragLeave = Com::Wui::Framework::Microcore::MSVC::Callbacks::OnDragLeave;
        using OnDragOver = Com::Wui::Framework::Microcore::MSVC::Callbacks::OnDragOver;
        using OnDragEnter = Com::Wui::Framework::Microcore::MSVC::Callbacks::OnDragEnter;

     public:
        static DragAndDropBridge &GetInstance();

     private:
        DragAndDropBridge();

        DragAndDropBridge(const DragAndDropBridge &) = delete;

        DragAndDropBridge(DragAndDropBridge &&) = delete;

        DragAndDropBridge &operator=(const DragAndDropBridge &) = delete;

        DragAndDropBridge &operator=(DragAndDropBridge &&) = delete;

     public:
        function<bool(const char *,
                      OnDrop,
                      OnDragLeave,
                      OnDragOver,
                      OnDragEnter,
                      HWND)> Initialize = nullptr;

        function<void(HWND)> Destroy = nullptr;

        function<bool()> HasTarget = nullptr;

        function<CefBrowserHost::DragOperationsMask(Com::Wui::Framework::Microcore::MSVC::Structures::DragData,
                                                    CefRenderHandler::DragOperationsMask,
                                                    int,
                                                    int)> StartDragging = nullptr;

     private:
        void loadDragAndDropFunctions();

        Com::Wui::Framework::Microcore::Commons::SharedLibraryManager dragAndDropSharedLibrary;
    };
}

#endif   // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_MICROCORE_BRIDGES_DRAGANDDROPBRIDGE_WINDOWS_HPP_
