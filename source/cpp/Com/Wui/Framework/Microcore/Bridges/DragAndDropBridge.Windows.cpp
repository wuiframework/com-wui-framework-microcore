/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Bridges {
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    using DragData = Com::Wui::Framework::Microcore::MSVC::Structures::DragData;

    DragAndDropBridge &DragAndDropBridge::GetInstance() {
        static DragAndDropBridge instance;

        return instance;
    }

    DragAndDropBridge::DragAndDropBridge()
            : dragAndDropSharedLibrary("com-wui-framework-microcore-msvc.dll") {
        this->loadDragAndDropFunctions();
    }

    void DragAndDropBridge::loadDragAndDropFunctions() {
        LogIt::Info("Loading functions from the shared library");

        this->Initialize = dragAndDropSharedLibrary.GetProcedure<bool(const char *,
                                                                      OnDrop,
                                                                      OnDragLeave,
                                                                      OnDragOver,
                                                                      OnDragEnter,
                                                                      HWND)>("Initialize");

        this->Destroy = dragAndDropSharedLibrary.GetProcedure<void(HWND)>("Destroy");

        this->HasTarget = dragAndDropSharedLibrary.GetProcedure<bool()>("HasTarget");

        this->StartDragging = dragAndDropSharedLibrary.GetProcedure<CefBrowserHost::DragOperationsMask(DragData,
                                                                                                      CefRenderHandler::DragOperationsMask,
                                                                                                      int,
                                                                                                      int)>("StartDragging");
    }
}

#endif   // WIN_PLATFORM
