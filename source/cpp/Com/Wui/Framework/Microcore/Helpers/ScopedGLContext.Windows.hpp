/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_HELPERS_SCOPEDGLCONTEXT_WINDOWS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_HELPERS_SCOPEDGLCONTEXT_WINDOWS_HPP_

#ifdef WIN_PLATFORM

namespace Com::Wui::Framework::Microcore::Helpers {
    class ScopedGLContext {
     public:
        ScopedGLContext(HDC $hdc, HGLRC $hglrc, bool $swapBuffers);

        ~ScopedGLContext();

     private:
        HDC hdc = nullptr;
        bool swapBuffers = false;
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_MICROCORE_HELPERS_SCOPEDGLCONTEXT_WINDOWS_HPP_
