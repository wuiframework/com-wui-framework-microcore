/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Helpers::Utils {
    int DeviceToLogical(const int $value, const float $deviceScaleFactor) {
        float scaledValue = static_cast<float>($value) / $deviceScaleFactor;
        return static_cast<int>(std::floor(scaledValue));
    }

    void DeviceToLogical(CefMouseEvent &$value, const float $deviceScaleFactor) {
        $value.x = DeviceToLogical($value.x, $deviceScaleFactor);
        $value.y = DeviceToLogical($value.y, $deviceScaleFactor);
    }

    int LogicalToDevice(const int $value, const float $deviceScaleFactor) {
        float scaledValue = static_cast<float>($value) * $deviceScaleFactor;
        return static_cast<int>(std::floor(scaledValue));
    }

    CefRect LogicalToDevice(const CefRect &$value, const float $deviceScaleFactor) {
        return CefRect(LogicalToDevice($value.x, $deviceScaleFactor),
                       LogicalToDevice($value.y, $deviceScaleFactor),
                       LogicalToDevice($value.width, $deviceScaleFactor),
                       LogicalToDevice($value.height, $deviceScaleFactor));
    }
}
