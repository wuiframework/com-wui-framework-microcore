/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_HELPERS_UTILS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_HELPERS_UTILS_HPP_

#ifdef WIN_PLATFORM
#include "../Helpers/Utils.Windows.hpp"
#endif  // WIN_PLATFORM

namespace Com::Wui::Framework::Microcore::Helpers::Utils {
    int LogicalToDevice(int $value, float $deviceScaleFactor);

    CefRect LogicalToDevice(const CefRect &$value, float $deviceScaleFactor);

    int DeviceToLogical(int $value, float $deviceScaleFactor);

    void DeviceToLogical(CefMouseEvent &$value, float $deviceScaleFactor);
}
#endif  // COM_WUI_FRAMEWORK_MICROCORE_HELPERS_UTILS_HPP_
