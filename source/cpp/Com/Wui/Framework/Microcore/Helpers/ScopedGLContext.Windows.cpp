/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Helpers {
    ScopedGLContext::ScopedGLContext(const HDC $hdc, const HGLRC $hglrc, const bool $swapBuffers)
            : hdc($hdc),
              swapBuffers($swapBuffers) {
        const BOOL result = wglMakeCurrent($hdc, $hglrc);
        ALLOW_UNUSED_LOCAL(result);
        DCHECK(result);
    }

    ScopedGLContext::~ScopedGLContext() {
        BOOL result = wglMakeCurrent(NULL, NULL);
        DCHECK(result);
        if (swapBuffers) {
            result = SwapBuffers(this->hdc);
            DCHECK(result);
        }
    }
}

#endif  // WIN_PLATFORM
