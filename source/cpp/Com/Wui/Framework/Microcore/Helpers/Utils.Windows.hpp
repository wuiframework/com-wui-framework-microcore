/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_HELPERS_UTILS_WINDOWS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_HELPERS_UTILS_WINDOWS_HPP_

#ifdef WIN_PLATFORM

namespace Com::Wui::Framework::Microcore::Helpers::Utils {
    void SetUserDataPtr(const HWND $hwnd, void *$ptr);

    template<typename T>
    T GetUserDataPtr(const HWND $hwnd) {
        return reinterpret_cast<T>(GetWindowLongPtr($hwnd, GWLP_USERDATA));
    }

    CefMouseEvent GetCefMouseEvent(POINT $point);

    int GetCefMouseModifiers(WPARAM $wparam);

    int GetCefKeyboardModifiers(WPARAM $wparam, LPARAM $lparam);

    bool IsKeyDown(WPARAM $wparam);

    LPCWSTR GetPointerToWideString(const string &$string);
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_MICROCORE_HELPERS_UTILS_WINDOWS_HPP_
