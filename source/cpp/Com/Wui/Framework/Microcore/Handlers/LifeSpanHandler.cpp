/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Handlers {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;

    LifeSpanHandler::LifeSpanHandler(IOsrDelegate *$osrDelegate)
            : osrDelegate{$osrDelegate} {
        LogIt::Info("LifeSpanHandler is being constructed");
    }

    void LifeSpanHandler::OnAfterCreated(const CefRefPtr <CefBrowser> $browser) {
        CEF_REQUIRE_UI_THREAD();

        if (this->osrDelegate) {
            this->osrDelegate->OnAfterCreated($browser);
        }
    }

    void LifeSpanHandler::OnBeforeClose(CefRefPtr<CefBrowser> /*$browser*/) {
        CEF_REQUIRE_UI_THREAD();

        if (this->osrDelegate) {
            this->osrDelegate->OnBeforeClose();
        }

        CefQuitMessageLoop();
    }
}
