/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_LIFESPANHANDLER_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_LIFESPANHANDLER_HPP_

namespace Com::Wui::Framework::Microcore::Handlers {
    class LifeSpanHandler : public CefLifeSpanHandler {
        using IOsrDelegate = Com::Wui::Framework::Microcore::Interfaces::IOsrDelegate;

     public:
        explicit LifeSpanHandler(IOsrDelegate *$osrDelegate);

     private:
        void OnAfterCreated(CefRefPtr <CefBrowser> $browser) override;

        void OnBeforeClose(CefRefPtr <CefBrowser> $browser) override;

        IOsrDelegate *osrDelegate = nullptr;

        IMPLEMENT_REFCOUNTING(LifeSpanHandler);
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_LIFESPANHANDLER_HPP_
