/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_BROWSERPROCESSHANDLER_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_BROWSERPROCESSHANDLER_HPP_

namespace Com::Wui::Framework::Microcore::Handlers {
    class BrowserProcessHandler : public CefBrowserProcessHandler {
     public:
        using OnBrowserProcessReady = function<void()>;

        explicit BrowserProcessHandler(OnBrowserProcessReady $onBrowserProcessReadyCallback);

     private:
        void OnContextInitialized() override;

        OnBrowserProcessReady onBrowserProcessReadyCallback = nullptr;

        IMPLEMENT_REFCOUNTING(BrowserProcessHandler);
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_BROWSERPROCESSHANDLER_HPP_
