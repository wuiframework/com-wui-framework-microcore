/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_DISPLAYHANDLER_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_DISPLAYHANDLER_HPP_

namespace Com::Wui::Framework::Microcore::Handlers {
    class DisplayHandler : public CefDisplayHandler {
     private:
        IMPLEMENT_REFCOUNTING(DisplayHandler);
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_HANDLERS_DISPLAYHANDLER_HPP_
