/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Exceptions {
    SharedLibraryManager::ProcedureNotExists::ProcedureNotExists(const string &$name)
            : std::runtime_error{"Given procedure doesn't exist: " + $name} {
    }

    SharedLibraryManager::LibraryLoadFailed::LibraryLoadFailed(const string &$libraryPath)
            : std::runtime_error{"Could not load library or its dependencies: " + $libraryPath} {
    }
}
