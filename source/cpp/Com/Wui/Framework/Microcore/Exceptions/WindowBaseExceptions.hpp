/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_EXCEPTIONS_WINDOWBASEEXCEPTIONS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_EXCEPTIONS_WINDOWBASEEXCEPTIONS_HPP_

namespace Com::Wui::Framework::Microcore::Exceptions::WindowBase {
    struct FailedToInitialize : public std::runtime_error {
        explicit FailedToInitialize(const string &$id, const string &$reason);
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_EXCEPTIONS_WINDOWBASEEXCEPTIONS_HPP_
