/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Exceptions {
    WindowBase::FailedToInitialize::FailedToInitialize(const string &$id, const string &$reason)
            : std::runtime_error{"Failed to initialize window " + $id + " because of: " + $reason} {
    }
}
