/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOWBASE_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOWBASE_HPP_

#include "../Interfaces/IOsrDelegate.hpp"
#include "../Renderer/OsrRenderer.hpp"
#include "../Structures/WindowSettings.hpp"

namespace Com::Wui::Framework::Microcore::Browser {
    class WindowBase : public Com::Wui::Framework::Microcore::Interfaces::IOsrDelegate {
        using WindowSettings = Com::Wui::Framework::Microcore::Structures::WindowSettings;

     public:
        explicit WindowBase(WindowSettings $settings);

        ~WindowBase();

        virtual void Init() = 0;

        virtual void Destroy() = 0;

        void CreateBrowser();

        void OnAfterCreated(CefRefPtr<CefBrowser> $browser) override;

        void OnBeforeClose() override;

        void UpdateDragCursor(CefRenderHandler::DragOperation $operation) override;

        virtual void EnableGL() = 0;

        virtual void DisableGL() = 0;

        virtual void Render() = 0;

        void Invalidate();

        bool OnEraseBackground();

        string getId() const;

        void ApplyPopupOffset(int &$x, int &$y) const;

        CefBrowserHost::DragOperationsMask OnDragEnter(CefMouseEvent $event,
                                                       CefRefPtr<CefDragData> $dragData,
                                                       CefBrowserHost::DragOperationsMask $effect);

        CefBrowserHost::DragOperationsMask OnDragOver(CefMouseEvent $event, CefBrowserHost::DragOperationsMask $effect);

        void OnDragLeave();

        CefBrowserHost::DragOperationsMask OnDrop(CefMouseEvent $event, CefBrowserHost::DragOperationsMask $effect);

     protected:
        bool isOverPopupWidget(int $x, int $y) const;

        int getPopupXOffset() const;

        int getPopupYOffset() const;

        CefWindowInfo windowInfo;
        CefRenderHandler::DragOperation currentDragOperation = CefRenderHandler::DragOperation::DRAG_OPERATION_NONE;
        CefBrowserHost::MouseButtonType lastClickButton = CefBrowserHost::MouseButtonType::MBT_LEFT;
        WindowSettings settings;
        CefRefPtr<CefBrowser> browser = nullptr;
        Com::Wui::Framework::Microcore::Renderer::OsrRenderer renderer;
        float deviceScaleFactor = 1.0f;

     private:
        CefRefPtr<Com::Wui::Framework::Microcore::App::Client> client = nullptr;
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOWBASE_HPP_
