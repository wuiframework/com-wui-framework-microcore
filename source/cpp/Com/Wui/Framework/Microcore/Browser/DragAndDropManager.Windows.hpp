/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_BROWSER_DRAGANDDROPMANAGER_WINDOWS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_BROWSER_DRAGANDDROPMANAGER_WINDOWS_HPP_

#ifdef WIN_PLATFORM

namespace Com::Wui::Framework::Microcore::Browser {
    /**
     * Class manages drag-and-drop function calls from the microcore-msvc and forwards these to respective Windows.
     * The class is deliberately static, because it has to have fixed position in memory when program is started,
     * since microcore-msvc calls its methods and their addresses are passed when microcore-msvc is loaded
     * into the address space of microcore.
     */
    class DragAndDropManager {
        using WindowManager = Com::Wui::Framework::Microcore::Browser::WindowManager;

     public:
        static void SetWindowManager(WindowManager *$windowManager);

        static CefBrowserHost::DragOperationsMask OnDragEnter(const char *$id,
                                                              Com::Wui::Framework::Microcore::MSVC::Structures::DragData $dragData,
                                                              int $x,
                                                              int $y,
                                                              uint32 $modifiers,
                                                              CefBrowserHost::DragOperationsMask $effect);

        static void OnDragLeave(const char *$id);

        static CefBrowserHost::DragOperationsMask OnDrop(const char *$id,
                                                         int $x,
                                                         int $y,
                                                         int $modifiers,
                                                         CefBrowserHost::DragOperationsMask $effect);

        static CefBrowserHost::DragOperationsMask OnDragOver(const char *$id,
                                                             int $x,
                                                             int $y,
                                                             int $modifiers,
                                                             CefBrowserHost::DragOperationsMask $effect);

     private:
        static void assertWindowManager();

        static WindowManager *windowManager;
        static std::mutex concurrencyProtection;
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_MICROCORE_BROWSER_DRAGANDDROPMANAGER_WINDOWS_HPP_
