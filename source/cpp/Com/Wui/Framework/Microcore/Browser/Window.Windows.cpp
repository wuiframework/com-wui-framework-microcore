/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include <MSVC/Callbacks/Callbacks.hpp>
#include <MSVC/Structures/Conversions.hpp>
#include <MSVC/Structures/DragData.hpp>

#include "../sourceFilesMap.hpp"

// has to be here, because of the same symbols defined in the CEF
#include <windowsx.h> // NOLINT

namespace Com::Wui::Framework::Microcore::Browser {
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    using Com::Wui::Framework::Microcore::Bridges::DragAndDropBridge;
    using Com::Wui::Framework::Microcore::Browser::DragAndDropManager;
    using Com::Wui::Framework::Microcore::Browser::WindowBase;
    using Com::Wui::Framework::Microcore::Helpers::ScopedGLContext;

    namespace Exceptions = Com::Wui::Framework::Microcore::Exceptions::WindowBase;
    namespace Utils = Com::Wui::Framework::Microcore::Helpers::Utils;

    Window::Window(WindowSettings $settings)
            : WindowBase{std::move($settings)} {
    }

    void Window::Init() {
        LogIt::Info("Window {0} is being initialized", this->settings.id);

        this->CreateWin32Window();
        this->SetWindowProcessingCallback();
        UpdateWindow(this->hwnd);
        GetWindowRect(this->hwnd, &clientRect);
        Utils::SetUserDataPtr(this->hwnd, this);
        this->windowInfo.SetAsWindowless(this->hwnd);

        if (DragAndDropBridge::GetInstance().Initialize(this->settings.id.c_str(),
                                                        reinterpret_cast<MSVC::Callbacks::OnDrop>(&DragAndDropManager::OnDrop),
                                                        reinterpret_cast<MSVC::Callbacks::OnDragLeave>(&DragAndDropManager::OnDragLeave),
                                                        reinterpret_cast<MSVC::Callbacks::OnDragOver>(&DragAndDropManager::OnDragOver),
                                                        reinterpret_cast<MSVC::Callbacks::OnDragEnter>(&DragAndDropManager::OnDragEnter),
                                                        this->hwnd)) {
            LogIt::Info("Drag-and-drop functionality successfully initialized");
        } else {
            throw Exceptions::FailedToInitialize{this->getId(), "drag and drop functionality failed to initialize"};
        }
    }

    void Window::Destroy() {
        LogIt::Info("Destroying window");

        DragAndDropBridge::GetInstance().Destroy(this->hwnd);

        this->DisableGL();

        DestroyWindow(this->hwnd);
        this->hwnd = nullptr;
    }

    void Window::OnPaint(const CefRenderHandler::PaintElementType $type,
                         const CefRenderHandler::RectList &$dirtyRects,
                         const void *$buffer,
                         const int $width,
                         const int $height) {
        if (!this->hdc) {
            this->EnableGL();
        }

        ScopedGLContext scopedGlContext(this->hdc, this->hrc, true);
        this->renderer.OnPaint($type, $dirtyRects, $buffer, $width, $height);
        if ($type == PET_VIEW && !this->renderer.getPopupRect().IsEmpty()) {
            this->browser->GetHost()->Invalidate(PET_POPUP);
        }
        this->renderer.Render();
    }

    bool Window::GetViewRect(CefRect &$rect) {
        $rect.x = clientRect.left;
        $rect.y = clientRect.top;

        $rect.width = Utils::DeviceToLogical(clientRect.right, deviceScaleFactor);
        $rect.height = Utils::DeviceToLogical(clientRect.bottom, deviceScaleFactor);

        return true;
    }

    bool Window::GetScreenPoint(const int $viewX, const int $viewY, int &$screenX, int &$screenY) {
        if (!IsWindow(this->hwnd)) {
            return false;
        }

        POINT screen_pt = {Utils::LogicalToDevice($viewX, this->deviceScaleFactor),
                           Utils::LogicalToDevice($viewY, this->deviceScaleFactor)};
        ClientToScreen(this->hwnd, &screen_pt);
        $screenX = screen_pt.x;
        $screenY = screen_pt.y;

        return true;
    }

    void Window::OnCursorChange(const CefCursorHandle $cursor) {
        if (!IsWindow(this->hwnd)) {
            return;
        }

        SetClassLongPtr(this->hwnd, GCLP_HCURSOR, static_cast<LONG>(reinterpret_cast<LONG_PTR>($cursor)));
        SetCursor($cursor);
    }

    bool Window::StartDragging(const CefRefPtr<CefBrowser> $browser,
                               const CefRefPtr<CefDragData> $cefDragData,
                               const CefRenderHandler::DragOperationsMask $allowedOps,
                               const int $x,
                               const int $y) {
        CEF_REQUIRE_UI_THREAD();

        if (DragAndDropBridge::GetInstance().HasTarget()) {
            return false;
        }

        currentDragOperation = DRAG_OPERATION_NONE;
        MSVC::Structures::DragData dragData = MSVC::Structures::CreateDragData($cefDragData);
        const CefBrowserHost::DragOperationsMask result = DragAndDropBridge::GetInstance().StartDragging(dragData,
                                                                                                         $allowedOps,
                                                                                                         $x,
                                                                                                         $y);
        MSVC::Structures::DestroyDragData(&dragData);

        currentDragOperation = DRAG_OPERATION_NONE;
        POINT pt = {0, 0};
        GetCursorPos(&pt);
        ScreenToClient(this->hwnd, &pt);

        $browser->GetHost()->DragSourceEndedAt(Utils::DeviceToLogical(pt.x, this->deviceScaleFactor),
                                               Utils::DeviceToLogical(pt.y, this->deviceScaleFactor),
                                               result);
        $browser->GetHost()->DragSourceSystemDragEnded();

        return true;
    }

    bool Window::SetWindowProcessingCallback() const {
        return SetWindowLongPtr(this->hwnd, GWLP_WNDPROC, (LONG_PTR)&Window::windowProcedureCallback) == 0 ? false : true;
    }

    void Window::CreateWin32Window() {
        WNDCLASSEXW windowClass;

        windowClass.cbSize = sizeof(WNDCLASSEX);

        windowClass.style = CS_OWNDC;
        windowClass.lpfnWndProc = DefWindowProc;
        windowClass.cbClsExtra = 0;
        windowClass.cbWndExtra = 0;
        windowClass.hInstance = GetModuleHandle(nullptr);
        windowClass.hIcon = nullptr;
        windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
        windowClass.hbrBackground = nullptr;
        windowClass.lpszMenuName = nullptr;
        windowClass.lpszClassName = Utils::GetPointerToWideString(this->settings.id);
        windowClass.hIconSm = nullptr;

        RegisterClassEx(&windowClass);

        DWORD style = WS_POPUPWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

        if (this->settings.visible) {
            style |= WS_VISIBLE;
        }

        this->hwnd = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW,
                                    Utils::GetPointerToWideString(this->settings.id),
                                    Utils::GetPointerToWideString(this->settings.id),
                                    style,
                                    0,
                                    0,
                                    1024,
                                    768,
                                    nullptr,
                                    nullptr,
                                    GetModuleHandle(nullptr),
                                    nullptr);

        if (!this->hwnd) {
            throw Exceptions::FailedToInitialize{this->settings.id, "Could not create win32 window"};
        }
    }

    void Window::OnPaint() {
        PAINTSTRUCT ps;
        BeginPaint(this->hwnd, &ps);
        EndPaint(this->hwnd, &ps);

        if (this->browser) {
            this->browser->GetHost()->Invalidate(PET_VIEW);
        }
    }

    void Window::EnableGL() {
        PIXELFORMATDESCRIPTOR pfd;
        hdc = GetDC(hwnd);
        ZeroMemory(&pfd, sizeof(pfd));

        pfd.nSize = sizeof(pfd);
        pfd.nVersion = 1;
        pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        pfd.iPixelType = PFD_TYPE_RGBA;
        pfd.cColorBits = 24;
        pfd.cDepthBits = 16;
        pfd.iLayerType = PFD_MAIN_PLANE;
        const int format = ChoosePixelFormat(this->hdc, &pfd);
        SetPixelFormat(this->hdc, format, &pfd);

        this->hrc = wglCreateContext(this->hdc);

        ScopedGLContext scoped_gl_context(this->hdc, this->hrc, false);
        this->renderer.Initialize();
    }

    void Window::DisableGL() {
        if (!this->hdc) {
            return;
        }
        {
            ScopedGLContext scoped_gl_context(this->hdc, this->hrc, false);
            this->renderer.Cleanup();
        }

        if (IsWindow(this->hwnd)) {
            const BOOL result = wglDeleteContext(this->hrc);
            ALLOW_UNUSED_LOCAL(result);
            DCHECK(result);
            ReleaseDC(this->hwnd, this->hdc);
        }

        this->hdc = nullptr;
        this->hrc = nullptr;
    }

    void Window::Render() {
        if (!this->hdc) {
            this->EnableGL();
        }

        ScopedGLContext scoped_gl_context(this->hdc, this->hrc, true);
        this->renderer.Render();
    }

    void Window::OnSize() {
        GetClientRect(this->hwnd, &this->clientRect);

        if (this->browser) {
            this->browser->GetHost()->WasResized();
        }
    }

    void Window::OnKeyEvent(const UINT $message, const WPARAM $wParam, const LPARAM $lParam) {
        if (!this->browser) {
            return;
        }

        CefKeyEvent event;
        event.windows_key_code = $wParam;
        event.native_key_code = $lParam;
        event.is_system_key = $message == WM_SYSCHAR || $message == WM_SYSKEYDOWN || $message == WM_SYSKEYUP;

        if ($message == WM_KEYDOWN || $message == WM_SYSKEYDOWN) {
            event.type = KEYEVENT_RAWKEYDOWN;
        } else if ($message == WM_KEYUP || $message == WM_SYSKEYUP) {
            event.type = KEYEVENT_KEYUP;
        } else {
            event.type = KEYEVENT_CHAR;
        }
        event.modifiers = Utils::GetCefKeyboardModifiers($wParam, $lParam);

        this->browser->GetHost()->SendKeyEvent(event);
    }

    void Window::OnMouseEvent(const UINT $message, const WPARAM $wParam, const LPARAM $lParam) {
        CefRefPtr<CefBrowserHost> browserHost;
        if (this->browser) {
            browserHost = browser->GetHost();
        }

        LONG currentTime = 0;
        bool cancelPreviousClick = false;
        const POINT mousePoint = POINT { GET_X_LPARAM($lParam), GET_Y_LPARAM($lParam)};

        const CefBrowserHost::MouseButtonType btnType =
                ($message == WM_LBUTTONDOWN ? MBT_LEFT : ($message == WM_RBUTTONDOWN ? MBT_RIGHT : MBT_MIDDLE));

        if ($message == WM_LBUTTONDOWN || $message == WM_RBUTTONDOWN ||
            $message == WM_MBUTTONDOWN || $message == WM_MOUSEMOVE ||
            $message == WM_MOUSELEAVE) {
            currentTime = GetMessageTime();
            cancelPreviousClick =
                    (abs(this->mouseInformation.lastClickX - mousePoint.x) > (GetSystemMetrics(SM_CXDOUBLECLK) / 2)) ||
                    (abs(this->mouseInformation.lastClickY - mousePoint.y) > (GetSystemMetrics(SM_CYDOUBLECLK) / 2)) ||
                    ((currentTime - this->mouseInformation.lastClickTime) > GetDoubleClickTime());
            if (cancelPreviousClick && ($message == WM_MOUSEMOVE || $message == WM_MOUSELEAVE)) {
                this->mouseInformation.lastClickCount = 0;
                this->mouseInformation.lastClickX = 0;
                this->mouseInformation.lastClickY = 0;
                this->mouseInformation.lastClickTime = 0;
            }
        }

        switch ($message) {
            case WM_LBUTTONDOWN:
            case WM_RBUTTONDOWN:
            case WM_MBUTTONDOWN: {
                SetCapture(hwnd);
                SetFocus(hwnd);

                if (!cancelPreviousClick && (btnType == lastClickButton)) {
                    ++this->mouseInformation.lastClickCount;
                } else {
                    this->mouseInformation.lastClickCount = 1;
                    this->mouseInformation.lastClickX = mousePoint.x;
                    this->mouseInformation.lastClickY = mousePoint.y;
                }
                this->mouseInformation.lastClickTime = currentTime;
                lastClickButton = btnType;

                if (browserHost) {
                    CefMouseEvent mouseEvent = Utils::GetCefMouseEvent(mousePoint);
                    this->mouseInformation.lastMouseDownOnView = !isOverPopupWidget(mousePoint.x, mousePoint.y);
                    this->ApplyPopupOffset(mouseEvent.x, mouseEvent.y);
                    Utils::DeviceToLogical(mouseEvent, this->deviceScaleFactor);
                    mouseEvent.modifiers = Utils::GetCefMouseModifiers($wParam);
                    browserHost->SendMouseClickEvent(mouseEvent, btnType, false, this->mouseInformation.lastClickCount);
                }
            }
                break;

            case WM_LBUTTONUP:
            case WM_RBUTTONUP:
            case WM_MBUTTONUP:
                if (GetCapture() == this->hwnd) {
                    ReleaseCapture();
                }
                if (browserHost) {
                    CefMouseEvent mouseEvent = Utils::GetCefMouseEvent(mousePoint);
                    if (this->mouseInformation.lastMouseDownOnView && isOverPopupWidget(mousePoint.x, mousePoint.y) &&
                       (this->getPopupXOffset() || this->getPopupYOffset())) {
                           break;
                    }
                    this->ApplyPopupOffset(mouseEvent.x, mouseEvent.y);
                    Utils::DeviceToLogical(mouseEvent, deviceScaleFactor);
                    mouseEvent.modifiers = Utils::GetCefMouseModifiers($wParam);
                    browserHost->SendMouseClickEvent(mouseEvent, btnType, true, this->mouseInformation.lastClickCount);
                }
                break;

            case WM_MOUSEMOVE: {
                if (!this->mouseInformation.mouseTracking) {
                    TRACKMOUSEEVENT tme;
                    tme.cbSize = sizeof(TRACKMOUSEEVENT);
                    tme.dwFlags = TME_LEAVE;
                    tme.hwndTrack = hwnd;
                    TrackMouseEvent(&tme);
                    this->mouseInformation.mouseTracking = true;
                }

                if (browserHost) {
                    CefMouseEvent mouseEvent = Utils::GetCefMouseEvent(mousePoint);
                    this->ApplyPopupOffset(mouseEvent.x, mouseEvent.y);
                    Utils::DeviceToLogical(mouseEvent, this->deviceScaleFactor);
                    mouseEvent.modifiers = Utils::GetCefMouseModifiers($wParam);
                    browserHost->SendMouseMoveEvent(mouseEvent, false);
                }
                break;
            }

            case WM_MOUSELEAVE: {
                if (this->mouseInformation.mouseTracking) {
                    TRACKMOUSEEVENT tme;
                    tme.cbSize = sizeof(TRACKMOUSEEVENT);
                    tme.dwFlags = TME_LEAVE & TME_CANCEL;
                    tme.hwndTrack = this->hwnd;
                    TrackMouseEvent(&tme);
                    this->mouseInformation.mouseTracking = false;
                }

                if (browserHost) {
                    POINT cursorPosition = {0, 0};
                    GetCursorPos(&cursorPosition);
                    ScreenToClient(this->hwnd, &cursorPosition);

                    CefMouseEvent mouseEvent = Utils::GetCefMouseEvent(cursorPosition);
                    Utils::DeviceToLogical(mouseEvent, this->deviceScaleFactor);
                    mouseEvent.modifiers = Utils::GetCefMouseModifiers($wParam);
                    browserHost->SendMouseMoveEvent(mouseEvent, true);
                }
            }
                break;

            case WM_MOUSEWHEEL:
                if (browserHost) {
                    POINT cursorPosition = {GET_X_LPARAM($lParam), GET_Y_LPARAM($lParam)};
                    const HWND scrolledWindow = WindowFromPoint(cursorPosition);
                    if (scrolledWindow != this->hwnd)
                        break;

                    ScreenToClient(this->hwnd, &cursorPosition);
                    const int delta = GET_WHEEL_DELTA_WPARAM($wParam);

                    CefMouseEvent mouseEvent = Utils::GetCefMouseEvent(cursorPosition);
                    this->ApplyPopupOffset(mouseEvent.x, mouseEvent.y);
                    Utils::DeviceToLogical(mouseEvent, this->deviceScaleFactor);
                    mouseEvent.modifiers = Utils::GetCefMouseModifiers($wParam);
                    browserHost->SendMouseWheelEvent(mouseEvent,
                                                     Utils::IsKeyDown(VK_SHIFT) ? delta : 0,
                                                     !Utils::IsKeyDown(VK_SHIFT) ? delta : 0);
                }
                break;
        }
    }

    LRESULT __stdcall Window::windowProcedureCallback(const HWND $hwnd, const UINT $message, const WPARAM $wParam, const LPARAM $lParam) {
        CEF_REQUIRE_UI_THREAD();

        Window *self = Utils::GetUserDataPtr<Window *>($hwnd);
        if (!self) {
            return DefWindowProc($hwnd, $message, $wParam, $lParam);
        }

        switch ($message) {
            case WM_LBUTTONDOWN:
            case WM_RBUTTONDOWN:
            case WM_MBUTTONDOWN:
            case WM_LBUTTONUP:
            case WM_RBUTTONUP:
            case WM_MBUTTONUP:
            case WM_MOUSEMOVE:
            case WM_MOUSELEAVE:
            case WM_MOUSEWHEEL:
                self->OnMouseEvent($message, $wParam, $lParam);
                break;

            case WM_SIZE:
                self->OnSize();
                break;

            case WM_SYSCHAR:
            case WM_SYSKEYDOWN:
            case WM_SYSKEYUP:
            case WM_KEYDOWN:
            case WM_KEYUP:
            case WM_CHAR:
                self->OnKeyEvent($message, $wParam, $lParam);
                break;

            case WM_PAINT:
                self->OnPaint();
                break;

            case WM_ERASEBKGND:
                if (self->OnEraseBackground()) {
                    break;
                }
                return 0;

            case WM_NCDESTROY:
                Utils::SetUserDataPtr($hwnd, nullptr);
                break;

            case WM_CLOSE:
                self->browser->GetHost()->CloseBrowser(true);
                break;

            default:
                break;
        }

        return DefWindowProc($hwnd, $message, $wParam, $lParam);
    }
}

#endif  // WIN_PLATFORM
