/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Browser {
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    using Com::Wui::Framework::Microcore::Browser::DragAndDropManager;
    using Com::Wui::Framework::Microcore::Browser::Window;

    namespace Exceptions = Com::Wui::Framework::Microcore::Exceptions::WindowManager;

    WindowManager::WindowManager() {
        LogIt::Info("Window manager is being created");

        DragAndDropManager::SetWindowManager(this);
    }

    WindowManager::~WindowManager() {
        LogIt::Info("Window manager is being destroyed");

        std::lock_guard<decltype(this->concurrencyProtection)> criticalSection{this->concurrencyProtection};

        this->windows.shrink_to_fit();
    }

    void WindowManager::RegisterNewWindow(WindowSettings $settings) {
        std::lock_guard<decltype(this->concurrencyProtection)> criticalSection{this->concurrencyProtection};

        LogIt::Info("Window manager is registering new window");

        auto window = this->createWindowInstance(std::move($settings));
        window->Init();
        window->CreateBrowser();

        this->windows.emplace_back(std::move(window));
    }

    WindowBase *WindowManager::GetWindow(const string &$id) const {
        std::lock_guard<decltype(this->concurrencyProtection)> criticalSection{this->concurrencyProtection};

        LogIt::Debug("Getting window for id {0}", $id);

        const auto iterator = std::find_if(std::cbegin(this->windows),
                                           std::cend(this->windows),
                                           [&$id](const unique_ptr<WindowBase> &window) {
                                               return window->getId() == $id;
                                           });

        if (iterator != std::cend(this->windows)) {
            return (*iterator).get();
        } else {
            throw Exceptions::WindowNotExists{$id};
        }
    }

    unique_ptr<WindowBase> WindowManager::createWindowInstance(WindowSettings $settings) const {
        return std::make_unique<Window>(std::move($settings));
    }
}
