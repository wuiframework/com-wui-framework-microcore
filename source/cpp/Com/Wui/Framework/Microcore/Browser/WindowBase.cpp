/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Browser {
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    using Com::Wui::Framework::Microcore::App::Client;
    using Com::Wui::Framework::Microcore::Commons::Configuration;
    using Com::Wui::Framework::Microcore::Interfaces::IOsrDelegate;
    using Com::Wui::Framework::Microcore::Structures::RendererSettings;

    namespace Utils = Com::Wui::Framework::Microcore::Helpers::Utils;

    WindowBase::WindowBase(WindowSettings $settings)
            : settings{std::move($settings)},
              renderer(RendererSettings{}),
              client(new Client{this}) {
        LogIt::Info("Window {0} is being created", this->settings.id);
    }

    WindowBase::~WindowBase() {
        LogIt::Info("Window {0} is being destroyed", this->settings.id);
    }

    void WindowBase::CreateBrowser() {
        CefBrowserSettings browserSettings;
        browserSettings.windowless_frame_rate = Configuration::GetInstance().getCefFrameRate();

        CefBrowserHost::CreateBrowser(this->windowInfo, this->client, this->settings.startingUrl, std::move(browserSettings), nullptr);
    }

    void WindowBase::OnAfterCreated(const CefRefPtr <CefBrowser> $browser) {
        this->browser = $browser;
        this->browser->GetHost()->SendFocusEvent(true);
    }

    void WindowBase::OnBeforeClose() {
        static_cast<Client *>(this->browser->GetHost()->GetClient().get())->DetachDelegate();
        this->browser = nullptr;
        this->Destroy();
    }

    void WindowBase::UpdateDragCursor(const CefRenderHandler::DragOperation $operation) {
        this->currentDragOperation = $operation;
    }

    void WindowBase::Invalidate() {
    }

    bool WindowBase::OnEraseBackground() {
        return (this->browser == nullptr);
    }

    string WindowBase::getId() const {
        return this->settings.id;
    }

    void WindowBase::ApplyPopupOffset(int &$x, int &$y) const {
        if (this->isOverPopupWidget($x, $y)) {
            $x += this->getPopupXOffset();
            $y += this->getPopupYOffset();
        }
    }

    CefBrowserHost::DragOperationsMask WindowBase::OnDragEnter(CefMouseEvent $event,
                                                               const CefRefPtr <CefDragData> $dragData,
                                                               const CefBrowserHost::DragOperationsMask $effect) {
        if (this->browser) {
            Utils::DeviceToLogical($event, this->deviceScaleFactor);
            this->browser->GetHost()->DragTargetDragEnter($dragData, $event, $effect);
            this->browser->GetHost()->DragTargetDragOver($event, $effect);
        }

        return this->currentDragOperation;
    }

    CefBrowserHost::DragOperationsMask WindowBase::OnDragOver(CefMouseEvent $event, const CefBrowserHost::DragOperationsMask $effect) {
        if (this->browser) {
            Utils::DeviceToLogical($event, this->deviceScaleFactor);
            this->browser->GetHost()->DragTargetDragOver($event, $effect);
        }

        return this->currentDragOperation;
    }

    void WindowBase::OnDragLeave() {
        if (this->browser) {
            this-> browser->GetHost()->DragTargetDragLeave();
        }
    }

    CefBrowserHost::DragOperationsMask WindowBase::OnDrop(CefMouseEvent $event, const CefBrowserHost::DragOperationsMask $effect) {
        if (this->browser) {
            Utils::DeviceToLogical($event, this->deviceScaleFactor);
            this->browser->GetHost()->DragTargetDragOver($event, $effect);
            this->browser->GetHost()->DragTargetDrop($event);
        }

        return this->currentDragOperation;
    }

    bool WindowBase::isOverPopupWidget(const int $x, const int $y) const {
        const CefRect &rc = this->renderer.getPopupRect();
        const int popupRight = rc.x + rc.width;
        const int popupBottom = rc.y + rc.height;

        return ($x >= rc.x) && ($x < popupRight) && ($y >= rc.y) && ($y < popupBottom);
    }

    int WindowBase::getPopupXOffset() const {
        return this->renderer.getOriginalPopupRect().x - this->renderer.getPopupRect().x;
    }

    int WindowBase::getPopupYOffset() const {
        return this->renderer.getOriginalPopupRect().y - this->renderer.getPopupRect().y;
    }
}
