/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOW_WINDOWS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOW_WINDOWS_HPP_

#ifdef WIN_PLATFORM

#include "../Browser/WindowBase.hpp"
#include "../Structures/MouseInformation.hpp"

namespace Com::Wui::Framework::Microcore::Browser {
    class Window : public WindowBase {
        using WindowSettings = Com::Wui::Framework::Microcore::Structures::WindowSettings;

     public:
        explicit Window(WindowSettings $settings);

        void Init() override;

        void Destroy() override;

        void OnPaint(CefRenderHandler::PaintElementType $type,
                     const CefRenderHandler::RectList &$dirtyRects,
                     const void *$buffer,
                     int $width,
                     int $height) override;

        bool GetViewRect(CefRect &$rect) override;

        bool GetScreenPoint(int $viewX, int $viewY, int &$screenX, int &$screenY) override;

        void OnCursorChange(CefCursorHandle $cursor) override;

        bool StartDragging(const CefRefPtr<CefBrowser> $browser,
                           const CefRefPtr<CefDragData> $cefDragData,
                           CefRenderHandler::DragOperationsMask $allowedOps,
                           int $x,
                           int $y) override;

        bool SetWindowProcessingCallback() const;

        void CreateWin32Window();

        void OnPaint();

        void EnableGL() override;

        void DisableGL() override;

        void Render() override;

        void OnSize();

        void OnKeyEvent(UINT $message, WPARAM $wParam, LPARAM $lParam);

        void OnMouseEvent(UINT $message, WPARAM $wParam, LPARAM $lParam);

     private:
        static LRESULT __stdcall windowProcedureCallback(const HWND $hwnd, UINT $message, WPARAM $wParam, LPARAM $lParam);

        HWND hwnd = nullptr;
        HDC hdc = nullptr;
        HGLRC hrc = nullptr;
        RECT clientRect = {0, 0, 0, 0};
        Com::Wui::Framework::Microcore::Structures::MouseInformation mouseInformation;
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOW_WINDOWS_HPP_
