/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOWMANAGER_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOWMANAGER_HPP_

namespace Com::Wui::Framework::Microcore::Browser {
    class WindowManager {
        using WindowBase = Com::Wui::Framework::Microcore::Browser::WindowBase;
        using WindowSettings = Com::Wui::Framework::Microcore::Structures::WindowSettings;

     public:
        WindowManager();

        virtual ~WindowManager();

        void RegisterNewWindow(WindowSettings $settings);

        WindowBase *GetWindow(const string &$id) const;

     private:
        unique_ptr <WindowBase> createWindowInstance(WindowSettings $settings) const;

        std::vector <unique_ptr<WindowBase>> windows;
        mutable std::mutex concurrencyProtection;
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_BROWSER_WINDOWMANAGER_HPP_
