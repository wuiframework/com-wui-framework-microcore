/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore {
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    using Com::Wui::Framework::Microcore::Enums::ProcessExitCodes;
    using Com::Wui::Framework::Microcore::Runners::Runner;

    int Application::Run(const int $argc, const char **$argv) {
        Runner::ProcessExitCode exitCode = ProcessExitCodes::DEFAULT;

        try {
            auto applicationRunner = std::make_unique<Runner>($argc, $argv);

            exitCode = applicationRunner->Run();
        } catch (const std::exception &exception) {
            LogIt::Error("Exception has been caught: {0}", exception.what());
        }

        LogIt::Info("Going to exit with code {0}", exitCode);

        return exitCode;
    }
}
