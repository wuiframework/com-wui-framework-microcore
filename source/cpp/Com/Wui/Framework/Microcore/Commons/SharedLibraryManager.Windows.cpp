/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Commons {
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    namespace Exceptions = Com::Wui::Framework::Microcore::Exceptions::SharedLibraryManager;

    SharedLibraryManager::SharedLibraryManager(const string &$libraryPath) {
        LogIt::Info("Loading shared library from {0}", $libraryPath);

        this->libraryHandle = LoadLibraryA($libraryPath.c_str());

        if (!this->libraryHandle) {
            throw Exceptions::LibraryLoadFailed{$libraryPath};
        }
    }

    SharedLibraryManager::~SharedLibraryManager() {
        if (this->libraryHandle != nullptr) {
            LogIt::Info("Freeing shared library");

            FreeLibrary(libraryHandle);
            this->libraryHandle = nullptr;
        }
    }
}

#endif  // WIN_PLATFORM
