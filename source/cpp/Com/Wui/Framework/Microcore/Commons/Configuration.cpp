/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Commons {
    Configuration &Configuration::GetInstance() {
        static Configuration instance;

        return instance;
    }

    string Configuration::getCefLogFilePath() const {
        return this->cefLogFilePath;
    }

    string Configuration::getCefCachePath() const {
        return this->cefCachePath;
    }

    cef_log_severity_t Configuration::getCefLogSeverity() const {
        return this->cefLogSeverity;
    }

    int Configuration::getCefFrameRate() const {
        return this->cefFrameRate;
    }

    int Configuration::getCefRemoteDebuggingPort() const {
        return this->cefRemoteDebuggingPort;
    }
}
