/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_COMMONS_SHAREDLIBRARYMANAGER_WINDOWS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_COMMONS_SHAREDLIBRARYMANAGER_WINDOWS_HPP_

#ifdef WIN_PLATFORM

#include "../Exceptions/SharedLibraryManagerExceptions.hpp"

namespace Com::Wui::Framework::Microcore::Commons {
    class SharedLibraryManager {
     public:
        explicit SharedLibraryManager(const string &$libraryPath);

        virtual ~SharedLibraryManager();

        template<typename T>
        struct TypeParser {
        };

        template<typename Ret, typename... Args>
        struct TypeParser<Ret(Args...)> {
            static function<Ret(Args...)> createFunction(const FARPROC procedureAddress) {
                return function<Ret(Args...)>(reinterpret_cast<Ret (__cdecl *)(Args...)>(procedureAddress)); // NOLINT
            }
        };

        template<typename FunctionType>
        function<FunctionType> GetProcedure(const string &$procedureName) {
            using Com::Wui::Framework::XCppCommons::Utils::LogIt;
            using Com::Wui::Framework::XCppCommons::Enums::LogLevel;

            LogIt::Info("GetProcedure '{0}'", $procedureName);

            function<FunctionType> procedure = nullptr;

            if (this->libraryHandle != nullptr) {
                const FARPROC procedureAddress = GetProcAddress(this->libraryHandle, $procedureName.c_str());

                if (procedureAddress == nullptr) {
                    throw Com::Wui::Framework::Microcore::Exceptions::SharedLibraryManager::ProcedureNotExists{$procedureName};
                } else {
                    procedure = TypeParser<FunctionType>::createFunction(procedureAddress);
                }
            }

            return procedure;
        }

     private:
        HMODULE libraryHandle = nullptr;
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_MICROCORE_COMMONS_SHAREDLIBRARYMANAGER_WINDOWS_HPP_
