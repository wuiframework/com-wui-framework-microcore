/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_COMMONS_CONFIGURATION_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_COMMONS_CONFIGURATION_HPP_

namespace Com::Wui::Framework::Microcore::Commons {
    class Configuration {
     public:
        static Configuration &GetInstance();

     private:
        Configuration() = default;

        Configuration(const Configuration &) = delete;

        Configuration(Configuration &&) = delete;

        Configuration &operator=(const Configuration &) = delete;

        Configuration &operator=(Configuration &&) = delete;

     public:
        string getCefLogFilePath() const;

        string getCefCachePath() const;

        cef_log_severity_t getCefLogSeverity() const;

        int getCefFrameRate() const;

        int getCefRemoteDebuggingPort() const;

     private:
        string cefLogFilePath = "Chromium.log";
        string cefCachePath = Com::Wui::Framework::XCppCommons::System::IO::FileSystem::getLocalAppDataPath() +
                              "/WUIFramework/" +
                              Com::Wui::Framework::XCppCommons::EnvironmentArgs::getInstance().getProjectName();
        cef_log_severity_t cefLogSeverity = cef_log_severity_t::LOGSEVERITY_VERBOSE;
        int cefFrameRate = 40;
        int cefRemoteDebuggingPort = 50023;
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_COMMONS_CONFIGURATION_HPP_
