/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_MOUSEINFORMATION_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_MOUSEINFORMATION_HPP_

namespace Com::Wui::Framework::Microcore::Structures {
    struct MouseInformation {
        bool mouseTracking = false;
        int lastClickCount = 0;
        int lastClickX = 0;
        int lastClickY = 0;
        double lastClickTime = 0.0;
        bool lastMouseDownOnView = false;
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_MOUSEINFORMATION_HPP_
