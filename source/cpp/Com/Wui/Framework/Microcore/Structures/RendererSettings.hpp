/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_RENDERERSETTINGS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_RENDERERSETTINGS_HPP_

namespace Com::Wui::Framework::Microcore::Structures {
    struct RendererSettings {
        bool showUpdateRect = false;
        cef_color_t backgroundColor = CefColorSetARGB(1, 255, 255, 255);
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_RENDERERSETTINGS_HPP_
