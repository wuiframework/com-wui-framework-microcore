/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_PROGRAMARGUMENTS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_PROGRAMARGUMENTS_HPP_

namespace Com::Wui::Framework::Microcore::Structures {
    class ProgramArguments
            : public Com::Wui::Framework::XCppCommons::Structures::ProgramArgs {
     public:
        ProgramArguments();

        bool getIsSingleProcess() const;

        bool getIsWindowVisible() const;

        string getUrl() const;

     private:
        bool singleProcess = false;
        bool windowVisible = true;
        string url = "https://google.com";
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_STRUCTURES_PROGRAMARGUMENTS_HPP_
