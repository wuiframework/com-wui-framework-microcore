/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Structures {
    ProgramArguments::ProgramArguments() {
        namespace po = boost::program_options;

        po::options_description appDescription("WuiMicrocore program options");

        appDescription.add_options()
                ("single-process", po::value<bool>(&this->singleProcess)->default_value(this->singleProcess),
                 "Specify whether embedded browser should run in a single process (1) or multiple processes (0)")
                ("window-visible", po::value<bool>(&this->windowVisible)->default_value(this->windowVisible),
                 "Initial window should be visible (1) or hidden")
                ("url", po::value<string>(&this->url)->default_value(this->url),
                 "Full starting URL, e.g. https://google.com");

        getDescription()->add(appDescription);
    }

    bool ProgramArguments::getIsSingleProcess() const {
        return this->singleProcess;
    }

    bool ProgramArguments::getIsWindowVisible() const {
        return this->windowVisible;
    }

    string ProgramArguments::getUrl() const {
        return this->url;
    }
}
