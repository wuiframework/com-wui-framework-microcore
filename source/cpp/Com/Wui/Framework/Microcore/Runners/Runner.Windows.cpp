/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::Runners {
    using Com::Wui::Framework::Microcore::Runners::RunnerBase;

    Runner::Runner(int $argc, const char **$argv)
            : RunnerBase{$argc, $argv} {
    }

    void Runner::initializeCefMainArgs() {
        this->cefMainArgs = CefMainArgs{GetModuleHandle(nullptr)};
    }
}

#endif  // WIN_PLATFORM
