/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_RUNNERS_RUNNER_LINUX_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_RUNNERS_RUNNER_LINUX_HPP_

#ifdef OS_LINUX

#include "../Runners/RunnerBase.hpp"

namespace Com::Wui::Framework::Microcore::Runners {
    class Runner : public RunnerBase {
     public:
        Runner(int $argc, const char **$argv);

     private:
        void initializeCefMainArgs() override;
    };
}

#endif  // OS_LINUX

#endif  // COM_WUI_FRAMEWORK_MICROCORE_RUNNERS_RUNNER_LINUX_HPP_
