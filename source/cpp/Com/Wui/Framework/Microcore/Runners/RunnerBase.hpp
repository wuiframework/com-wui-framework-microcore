/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_RUNNERS_RUNNERBASE_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_RUNNERS_RUNNERBASE_HPP_

namespace Com::Wui::Framework::Microcore::Runners {
    class RunnerBase {
     public:
        using ProcessExitCode = int;
        using ArgumentParsingResult = int;

        RunnerBase(int $argc, const char **$argv);

        virtual ~RunnerBase() = default;

        ProcessExitCode Run();

     protected:
        ProcessExitCode initialize();

        void runMessageLoop();

        void shutdown();

        ProcessExitCode launchSecondaryProcess();

        ArgumentParsingResult parseArguments();

        virtual void initializeCefMainArgs() = 0;

        CefSettings createCefSettings() const;

        void deleteCefLogFile();

        int argc = 0;
        const char **argv = nullptr;
        Com::Wui::Framework::Microcore::Structures::ProgramArguments programArguments;
        CefMainArgs cefMainArgs;
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_RUNNERS_RUNNERBASE_HPP_
