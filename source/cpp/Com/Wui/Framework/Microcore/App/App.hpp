/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_APP_APP_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_APP_APP_HPP_

#include "../Structures/ProgramArguments.hpp"

namespace Com::Wui::Framework::Microcore::App {
    class Application : public CefApp {
        using ProgramArguments = Com::Wui::Framework::Microcore::Structures::ProgramArguments;

     public:
        explicit Application(ProgramArguments $settings);

        CefRefPtr <CefBrowserProcessHandler> GetBrowserProcessHandler() override;

        void CreateApplicationWindow();

     private:
        unique_ptr <Com::Wui::Framework::Microcore::Browser::WindowManager> windowManager = nullptr;
        CefRefPtr <Com::Wui::Framework::Microcore::Handlers::BrowserProcessHandler> browserProcessHandler = nullptr;
        ProgramArguments settings;

        IMPLEMENT_REFCOUNTING(Application);
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_APP_APP_HPP_
