/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_APP_CLIENT_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_APP_CLIENT_HPP_

#include "../Handlers/DisplayHandler.hpp"
#include "../Handlers/DragHandler.hpp"
#include "../Handlers/LifeSpanHandler.hpp"
#include "../Handlers/LoadHandler.hpp"
#include "../Handlers/RenderHandler.hpp"

namespace Com::Wui::Framework::Microcore::App {
    class Client : public CefClient {
        using DisplayHandler = Com::Wui::Framework::Microcore::Handlers::DisplayHandler;
        using LifeSpanHandler = Com::Wui::Framework::Microcore::Handlers::LifeSpanHandler;
        using LoadHandler = Com::Wui::Framework::Microcore::Handlers::LoadHandler;
        using RenderHandler = Com::Wui::Framework::Microcore::Handlers::RenderHandler;
        using DragHandler = Com::Wui::Framework::Microcore::Handlers::DragHandler;
        using IOsrDelegate = Com::Wui::Framework::Microcore::Interfaces::IOsrDelegate;

     public:
        explicit Client(IOsrDelegate *$delegate);

        CefRefPtr <CefDisplayHandler> GetDisplayHandler() override;

        CefRefPtr <CefLifeSpanHandler> GetLifeSpanHandler() override;

        CefRefPtr <CefLoadHandler> GetLoadHandler() override;

        CefRefPtr <CefRenderHandler> GetRenderHandler() override;

        CefRefPtr <CefDragHandler> GetDragHandler() override;

        void DetachDelegate();

     private:
        IOsrDelegate *osrDelegate = nullptr;
        CefRefPtr <DisplayHandler> displayHandler = new DisplayHandler;
        CefRefPtr <LifeSpanHandler> lifeSpanHandler = new LifeSpanHandler{osrDelegate};
        CefRefPtr <LoadHandler> loadHandler = new LoadHandler;
        CefRefPtr <RenderHandler> renderHandler = new RenderHandler{osrDelegate};
        CefRefPtr <DragHandler> dragHandler = new DragHandler;

        IMPLEMENT_REFCOUNTING(Client);
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_APP_CLIENT_HPP_
