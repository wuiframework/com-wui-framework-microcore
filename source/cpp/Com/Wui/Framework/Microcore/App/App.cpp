/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::Microcore::App {
    using Com::Wui::Framework::Microcore::Browser::WindowManager;
    using Com::Wui::Framework::Microcore::Handlers::BrowserProcessHandler;
    using Com::Wui::Framework::Microcore::Structures::WindowSettings;

    Application::Application(ProgramArguments $settings)
            : windowManager{std::make_unique<WindowManager>()},
              settings{std::move($settings)} {
        this->browserProcessHandler = new BrowserProcessHandler{[this]() {
            this->CreateApplicationWindow();
        }};
    }

    CefRefPtr <CefBrowserProcessHandler> Application::GetBrowserProcessHandler() {
        return this->browserProcessHandler;
    }

    void Application::CreateApplicationWindow() {
        WindowSettings windowSettings;
        windowSettings.id = "com-wui-microcore-window1";
        windowSettings.startingUrl = settings.getUrl();
        windowSettings.visible = settings.getIsWindowVisible();

        this->windowManager->RegisterNewWindow(std::move(windowSettings));
    }
}
