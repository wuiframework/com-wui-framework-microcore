/* ********************************************************************************************************* *
 *
 * Copyright (c) 2012 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_RENDERER_OSRRENDERERMACROS_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_RENDERER_OSRRENDERERMACROS_HPP_

#if DCHECK_IS_ON()
#define VERIFY_NO_ERROR                                                      \
  {                                                                          \
    const int _gl_error = glGetError();                                      \
    DCHECK(_gl_error == GL_NO_ERROR) << "glGetError returned " << _gl_error; \
  }
#else
#define VERIFY_NO_ERROR
#endif

#endif  // COM_WUI_FRAMEWORK_MICROCORE_RENDERER_OSRRENDERERMACROS_HPP_
