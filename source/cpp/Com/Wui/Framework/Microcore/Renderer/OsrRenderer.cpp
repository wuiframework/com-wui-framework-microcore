/* ********************************************************************************************************* *
 *
 * Copyright (c) 2012 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

#ifdef WIN_PLATFORM
#include <gl/gl.h>
#elif OS_MACOSX
#include <OpenGL/gl.h>
#elif OS_LINUX
#include <GL/gl.h>
#else
#error Not implemented on this platform
#endif

namespace {
    const GLenum GL_BGR = 0x80E0;
    const GLenum GL_BGRA = 0x80E1;
    const GLenum GL_UNSIGNED_INT_8_8_8_8_REV = 0x8367;
}

namespace Com::Wui::Framework::Microcore::Renderer {
    OsrRenderer::OsrRenderer(RendererSettings $settings)
            : settings(std::move($settings)) {
    }

    OsrRenderer::~OsrRenderer() {
        this->Cleanup();
    }

    void OsrRenderer::Initialize() {
        if (this->initialized) {
            return;
        }

        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
        VERIFY_NO_ERROR;

        if (this->isTransparent()) {
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            VERIFY_NO_ERROR;
        } else {
            glClearColor(static_cast<float>(CefColorGetR(this->settings.backgroundColor)) / 255.0f,
                         static_cast<float>(CefColorGetG(this->settings.backgroundColor)) / 255.0f,
                         static_cast<float>(CefColorGetB(this->settings.backgroundColor)) / 255.0f,
                         1.0f);
            VERIFY_NO_ERROR;
        }

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        VERIFY_NO_ERROR;

        glGenTextures(1, &this->textureId);
        VERIFY_NO_ERROR;
        DCHECK_NE(this->textureId, 0U);
        VERIFY_NO_ERROR;

        glBindTexture(GL_TEXTURE_2D, this->textureId);
        VERIFY_NO_ERROR;
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        VERIFY_NO_ERROR;
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        VERIFY_NO_ERROR;
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        VERIFY_NO_ERROR;

        this->initialized = true;
    }

    void OsrRenderer::Cleanup() {
        if (this->textureId != 0)
            glDeleteTextures(1, &this->textureId);
    }

    void OsrRenderer::Render() {
        if (this->viewWidth == 0 || this->viewHeight == 0) {
            return;
        }

        DCHECK(this->initialized);

        struct {
            float tu, tv;
            float x, y, z;
        } static vertices[] = {{0.0f, 1.0f, -1.0f, -1.0f, 0.0f},
                               {1.0f, 1.0f, 1.0f,  -1.0f, 0.0f},
                               {1.0f, 0.0f, 1.0f,  1.0f,  0.0f},
                               {0.0f, 0.0f, -1.0f, 1.0f,  0.0f}};

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        VERIFY_NO_ERROR;

        glMatrixMode(GL_MODELVIEW);
        VERIFY_NO_ERROR;
        glLoadIdentity();
        VERIFY_NO_ERROR;

        glViewport(0, 0, this->viewWidth, this->viewHeight);
        VERIFY_NO_ERROR;
        glMatrixMode(GL_PROJECTION);
        VERIFY_NO_ERROR;
        glLoadIdentity();
        VERIFY_NO_ERROR;

        glPushAttrib(GL_ALL_ATTRIB_BITS);
        VERIFY_NO_ERROR;

        glBegin(GL_QUADS);
        glColor4f(1.0, 0.0, 0.0, 1.0);
        glVertex2f(-1.0, -1.0);
        glVertex2f(1.0, -1.0);
        glColor4f(0.0, 0.0, 1.0, 1.0);
        glVertex2f(1.0, 1.0);
        glVertex2f(-1.0, 1.0);
        glEnd();
        VERIFY_NO_ERROR;
        glPopAttrib();
        VERIFY_NO_ERROR;

        if (this->spinX != 0) {
            glRotatef(-this->spinX, 1.0f, 0.0f, 0.0f);
            VERIFY_NO_ERROR;
        }
        if (this->spinY != 0) {
            glRotatef(-this->spinY, 0.0f, 1.0f, 0.0f);
            VERIFY_NO_ERROR;
        }

        if (isTransparent()) {
            glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            VERIFY_NO_ERROR;

            glEnable(GL_BLEND);
            VERIFY_NO_ERROR;
        }

        glEnable(GL_TEXTURE_2D);
        VERIFY_NO_ERROR;

        DCHECK_NE(this->textureId, 0U);
        VERIFY_NO_ERROR;
        glBindTexture(GL_TEXTURE_2D, this->textureId);
        VERIFY_NO_ERROR;
        glInterleavedArrays(GL_T2F_V3F, 0, vertices);
        VERIFY_NO_ERROR;
        glDrawArrays(GL_QUADS, 0, 4);
        VERIFY_NO_ERROR;

        glDisable(GL_TEXTURE_2D);
        VERIFY_NO_ERROR;

        if (this->isTransparent()) {
            glDisable(GL_BLEND);
            VERIFY_NO_ERROR;
        }

        if (this->settings.showUpdateRect && !this->updateRect.IsEmpty()) {
            int left = this->updateRect.x;
            int right = this->updateRect.x + this->updateRect.width;
            int top = this->updateRect.y;
            int bottom = this->updateRect.y + this->updateRect.height;

#if defined(OS_LINUX)
            top += 1;
            right -= 1;
#else
            left += 1;
            bottom -= 1;
#endif

            glPushAttrib(GL_ALL_ATTRIB_BITS);
            VERIFY_NO_ERROR
            glMatrixMode(GL_PROJECTION);
            VERIFY_NO_ERROR;
            glPushMatrix();
            VERIFY_NO_ERROR;
            glLoadIdentity();
            VERIFY_NO_ERROR;
            glOrtho(0, this->viewWidth, this->viewHeight, 0, 0, 1);
            VERIFY_NO_ERROR;

            glLineWidth(1);
            VERIFY_NO_ERROR;
            glColor3f(1.0f, 0.0f, 0.0f);
            VERIFY_NO_ERROR;

            glBegin(GL_LINE_STRIP);
            glVertex2i(left, top);
            glVertex2i(right, top);
            glVertex2i(right, bottom);
            glVertex2i(left, bottom);
            glVertex2i(left, top);
            glEnd();
            VERIFY_NO_ERROR;

            glPopMatrix();
            VERIFY_NO_ERROR;
            glPopAttrib();
            VERIFY_NO_ERROR;
        }
    }

    void OsrRenderer::OnPopupShow(const bool $show) {
        if (!$show) {
            this->ClearPopupRects();
        }
    }

    void OsrRenderer::OnPopupSize(const CefRect &$rect) {
        if ($rect.width <= 0 || $rect.height <= 0) {
            return;
        }

        this->originalPopupRect = $rect;
        this->popupRect = this->getPopupRectInWebView(this->originalPopupRect);
    }

    void OsrRenderer::OnPaint(CefRenderHandler::PaintElementType $type,
                              const CefRenderHandler::RectList &$dirtyRects,
                              const void *$buffer,
                              int $width,
                              int $height) {
        if (!this->initialized) {
            this->Initialize();
        }

        if (this->isTransparent()) {
            glEnable(GL_BLEND);
            VERIFY_NO_ERROR;
        }

        glEnable(GL_TEXTURE_2D);
        VERIFY_NO_ERROR;

        DCHECK_NE(this->textureId, 0U);
        glBindTexture(GL_TEXTURE_2D, this->textureId);
        VERIFY_NO_ERROR;

        if ($type == PET_VIEW) {
            const int oldWidth = this->viewWidth;
            const int oldHeight = this->viewHeight;

            this->viewWidth = $width;
            this->viewHeight = $height;

            if (this->settings.showUpdateRect) {
                this->updateRect = $dirtyRects[0];
            }

            glPixelStorei(GL_UNPACK_ROW_LENGTH, this->viewWidth);
            VERIFY_NO_ERROR;

            if (oldWidth != this->viewWidth || oldHeight != this->viewHeight ||
                ($dirtyRects.size() == 1 && $dirtyRects[0] == CefRect(0, 0, this->viewWidth, this->viewHeight))) {
                glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
                VERIFY_NO_ERROR;
                glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
                VERIFY_NO_ERROR;
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->viewWidth, this->viewHeight, 0,
                             GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, $buffer);
                VERIFY_NO_ERROR;
            } else {
                CefRenderHandler::RectList::const_iterator i = $dirtyRects.begin();
                for (; i != $dirtyRects.end(); ++i) {
                    const CefRect &rect = *i;
                    DCHECK(rect.x + rect.width <= this->viewWidth);
                    DCHECK(rect.y + rect.height <= this->viewHeight);
                    glPixelStorei(GL_UNPACK_SKIP_PIXELS, rect.x);
                    VERIFY_NO_ERROR;
                    glPixelStorei(GL_UNPACK_SKIP_ROWS, rect.y);
                    VERIFY_NO_ERROR;
                    glTexSubImage2D(GL_TEXTURE_2D, 0, rect.x, rect.y, rect.width,
                                    rect.height, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV,
                                    $buffer);
                    VERIFY_NO_ERROR;
                }
            }
        } else if ($type == PET_POPUP && this->popupRect.width > 0 && this->popupRect.height > 0) {
            int skipPixels = 0;
            int skipRows = 0;
            int x = popupRect.x;
            int y = popupRect.y;
            int width = $width;
            int height = $height;

            if (x < 0) {
                skipPixels = -x;
                x = 0;
            }
            if (y < 0) {
                skipRows = -y;
                y = 0;
            }
            if (x + width > this->viewWidth) {
                width -= x + width - this->viewWidth;
            }
            if (y + height > this->viewHeight) {
                height -= y + height - this->viewHeight;
            }

            glPixelStorei(GL_UNPACK_ROW_LENGTH, $width);
            VERIFY_NO_ERROR;
            glPixelStorei(GL_UNPACK_SKIP_PIXELS, skipPixels);
            VERIFY_NO_ERROR;
            glPixelStorei(GL_UNPACK_SKIP_ROWS, skipRows);
            VERIFY_NO_ERROR;
            glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, GL_BGRA,
                            GL_UNSIGNED_INT_8_8_8_8_REV, $buffer);
            VERIFY_NO_ERROR;
        }

        glDisable(GL_TEXTURE_2D);
        VERIFY_NO_ERROR;

        if (this->isTransparent()) {
            glDisable(GL_BLEND);
            VERIFY_NO_ERROR;
        }
    }

    void OsrRenderer::ClearPopupRects() {
        this->popupRect.Set(0, 0, 0, 0);
        this->originalPopupRect.Set(0, 0, 0, 0);
    }

    const CefRect &OsrRenderer::getPopupRect() const {
        return this->popupRect;
    }

    const CefRect &OsrRenderer::getOriginalPopupRect() const {
        return this->originalPopupRect;
    }

    CefRect OsrRenderer::getPopupRectInWebView(const CefRect &$originalRect) {
        CefRect rc($originalRect);

        if (rc.x < 0) {
            rc.x = 0;
        }
        if (rc.y < 0) {
            rc.y = 0;
        }

        if (rc.x + rc.width > this->viewWidth) {
            rc.x = this->viewWidth - rc.width;
        }
        if (rc.y + rc.height > this->viewHeight) {
            rc.y = this->viewHeight - rc.height;
        }

        if (rc.x < 0) {
            rc.x = 0;
        }
        if (rc.y < 0) {
            rc.y = 0;
        }

        return rc;
    }

    bool OsrRenderer::isTransparent() const {
        return CefColorGetA(this->settings.backgroundColor) == 0;
    }
}
