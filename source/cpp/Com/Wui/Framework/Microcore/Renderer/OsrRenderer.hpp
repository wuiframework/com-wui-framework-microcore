/* ********************************************************************************************************* *
 *
 * Copyright (c) 2012 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_RENDERER_OSRRENDERER_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_RENDERER_OSRRENDERER_HPP_

#include "../Structures/RendererSettings.hpp"

namespace Com::Wui::Framework::Microcore::Renderer {
    class OsrRenderer {
        using RendererSettings = Com::Wui::Framework::Microcore::Structures::RendererSettings;

     public:
        explicit OsrRenderer(RendererSettings $settings);

        virtual ~OsrRenderer();

        void Initialize();

        void Cleanup();

        void Render();

        void OnPopupShow(bool $show);

        void OnPopupSize(const CefRect &$rect);

        void OnPaint(CefRenderHandler::PaintElementType $type,
                     const CefRenderHandler::RectList &$dirtyRects,
                     const void *$buffer,
                     int $width,
                     int $height);

        void ClearPopupRects();

        const CefRect &getPopupRect() const;

        const CefRect &getOriginalPopupRect() const;

        CefRect getPopupRectInWebView(const CefRect &$originalRect);

     private:
        bool isTransparent() const;

        RendererSettings settings;
        bool initialized = false;
        unsigned int textureId = 0;
        int viewWidth = 0;
        int viewHeight = 0;
        CefRect popupRect = { 0, 0, 0, 0};
        CefRect originalPopupRect = { 0, 0, 0, 0 };
        float spinX = 0;
        float spinY = 0;
        CefRect updateRect = { 0, 0, 0, 0};

        DISALLOW_COPY_AND_ASSIGN(OsrRenderer);
    };
}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_RENDERER_OSRRENDERER_HPP_
