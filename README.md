# com-wui-framework-microcore v2018.0.4

> Engine that provides web browser with off-screen rendering based in Chromium Embedded Framework.

## Requirements

This application depends on the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). 
See the WUI Builder requirements before you build this project.

This library has some special requirements:

* 7zip - for more information see the [readme.txt](resource/libs/7zip/readme.txt) file.
* winsdk - for more information see the [README.md](resource/libs/winsdk/README.md) file.

Other requirements are managed automatically through dependency manager:

* [com-wui-framework-xcppcommons](https://bitbucket.org/wuiframework/com-wui-framework-xcppcommons)
* [cef](https://bitbucket.org/chromiumembedded/cef) - a simple framework for embedding Chromium-based browser in other applications.

## Project build

This project build is fully automated. For more information about the project build, 
see the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

An interface with batch/bash scripts is prepared for Windows/Linux users for all common project tasks:

* `install`
* `build`
* `clean`
* `test`
* `run`
* `hotdeploy`
* `docs`

> NOTE: All batch scripts are stored in the **./bin/batch** (or ./bin/bash for linux) sub-folder in the project root folder.

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html) 
from the C++ source by running the `docs` command from the {projectRoot}/bin/batch folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2018.0.4

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Karel Burda
Copyright (c) 2018 [NXP](http://nxp.com/)
