# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 The Chromium Embedded Framework Authors
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

# Must be loaded via FindCEF.cmake.
if (NOT DEFINED _CEF_ROOT_EXPLICIT)
    message(FATAL_ERROR "Use find_package(CEF) to load this file.")
endif ()


#
# Shared configuration.
#

# Determine the platform.
if ("${CMAKE_SYSTEM_NAME}" STREQUAL "Darwin")
    set(OS_MACOSX 1)
    set(OS_POSIX 1)
elseif ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
    set(OS_LINUX 1)
    set(OS_POSIX 1)
elseif ("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
    set(OS_WINDOWS 1)
endif ()

# Determine the project architecture.
if (NOT DEFINED PROJECT_ARCH)
    if (CMAKE_SIZEOF_VOID_P MATCHES 8)
        set(PROJECT_ARCH "x86_64")
    else ()
        set(PROJECT_ARCH "x86")
    endif ()

    if (OS_MACOSX)
        # PROJECT_ARCH should be specified on Mac OS X.
        message(WARNING "No PROJECT_ARCH value specified, using ${PROJECT_ARCH}")
    endif ()
endif ()

if (OS_WINDOWS)

    macro(CREATE_POSIX_LIB_FROM_DLL dll_name dll_folder)
        message("Create/Update .DEF file for dll and export mapping posix library.")
        execute_process(COMMAND gendef ${dll_name}.dll WORKING_DIRECTORY ${dll_folder})
        execute_process(COMMAND dlltool --def ${dll_name}.def --dllname ${dll_name}.dll --output-lib ${dll_name}.a WORKING_DIRECTORY ${dll_folder})
    endmacro()

endif (OS_WINDOWS)

#
# Windows configuration.
#

if (OS_WINDOWS)
    # Standard libraries.
    set(CEF_STANDARD_LIBS
            comctl32.lib
            rpcrt4.lib
            shlwapi.lib
            ws2_32.lib)

    # CEF directory paths.
    set(CEF_RESOURCE_DIR ${_CEF_ROOT}/Resources)
    set(CEF_BINARY_DIR ${_CEF_ROOT}/${CMAKE_BUILD_TYPE})
    set(CEF_BINARY_DIR_DEBUG ${_CEF_ROOT}/Debug)
    set(CEF_BINARY_DIR_RELEASE ${_CEF_ROOT}/Release)

    # Update libcef.a library
    CREATE_POSIX_LIB_FROM_DLL(libcef ${CEF_BINARY_DIR_DEBUG})
    CREATE_POSIX_LIB_FROM_DLL(libcef ${CEF_BINARY_DIR_RELEASE})
    # CEF library paths.
    set(CEF_LIB_DEBUG ${CEF_BINARY_DIR_DEBUG}/libcef.a)
    set(CEF_LIB_RELEASE ${CEF_BINARY_DIR_RELEASE}/libcef.a)

    # List of CEF binary files.
    set(CEF_BINARY_FILES
            chrome_elf.dll
            libcef.dll
            libEGL.dll
            libGLESv2.dll
            natives_blob.bin
            snapshot_blob.bin
            v8_context_snapshot.bin)
    #
    # List of CEF resource files.
    set(CEF_RESOURCE_FILES
            cef.pak
            cef_100_percent.pak
            cef_200_percent.pak
            cef_extensions.pak
            devtools_resources.pak
            icudtl.dat
            locales)

endif ()
