# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 The Chromium Embedded Framework Authors
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

# Find the CEF binary distribution root directory and CEF-patch directory.
set(_CEF_ROOT "")
if (CEF_ROOT AND IS_DIRECTORY "${CEF_ROOT}")
    set(_CEF_ROOT "${CEF_ROOT}")
    set(_CEF_ROOT_EXPLICIT 1)
else ()
    set(_ENV_CEF_ROOT "")
    if (DEFINED ENV{CEF_ROOT})
        file(TO_CMAKE_PATH "$ENV{CEF_ROOT}" _ENV_CEF_ROOT)
    endif ()
    if (_ENV_CEF_ROOT AND IS_DIRECTORY "${_ENV_CEF_ROOT}")
        set(_CEF_ROOT "${_ENV_CEF_ROOT}")
        set(_CEF_ROOT_EXPLICIT 1)
    endif ()
    unset(_ENV_CEF_ROOT)
endif ()

if (NOT DEFINED _CEF_ROOT_EXPLICIT)
    message(FATAL_ERROR "Must specify a CEF_ROOT value via CMake or environment variable.")
endif ()

if (NOT IS_DIRECTORY "${_CEF_ROOT}/cmake")
    message(FATAL_ERROR "No CMake bootstrap found for CEF binary distribution at: ${CEF_ROOT}.")
endif ()

if (NOT IS_DIRECTORY "${CEF_PATCH_ROOT}/cmake")
    message(FATAL_ERROR "No CMake bootstrap found for CEF binary distribution patch at: ${CEF_PATH_ROOT}.")
endif ()

# Execute additional cmake files from the CEF binary distribution.
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CEF_PATCH_ROOT}/cmake")
include("cef_variables")
include("cef_macros")
