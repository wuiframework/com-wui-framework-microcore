/* ********************************************************************************************************* *
 *
 * Copyright (c) 2009 Marshall A. Greenblatt
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef CEF_INCLUDE_INTERNAL_CEF_EXPORT_H_
#define CEF_INCLUDE_INTERNAL_CEF_EXPORT_H_
#pragma once

#include "include/base/cef_build.h"

#if defined(COMPILER_MSVC)

#ifdef BUILDING_CEF_SHARED
#define CEF_EXPORT __declspec(dllexport)
#elif USING_CEF_SHARED
#define CEF_EXPORT __declspec(dllimport)
#else
#define CEF_EXPORT
#endif
#define CEF_CALLBACK __stdcall

#elif defined(COMPILER_GCC)

#if defined(OS_WIN)

#define CEF_EXPORT __attribute__ ((visibility("default")))
#define CEF_CALLBACK __stdcall

#else
#define CEF_EXPORT __attribute__ ((visibility("default")))
#define CEF_CALLBACK
#endif

#endif  // COMPILER_GCC

#endif  // CEF_INCLUDE_INTERNAL_CEF_EXPORT_H_
