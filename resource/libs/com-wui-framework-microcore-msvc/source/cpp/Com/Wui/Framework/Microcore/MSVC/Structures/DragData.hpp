/* ********************************************************************************************************* *
*
* Copyright (c) 2018 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_MSVC_STRUCTURES_DRAGDATA_HPP_
#define COM_WUI_FRAMEWORK_MICROCORE_MSVC_STRUCTURES_DRAGDATA_HPP_

#include <cstdint>

#include <include/cef_browser.h>

namespace Com { namespace Wui { namespace Framework { namespace Microcore { namespace MSVC { namespace Structures {

    // POD data-types because of GCC-MSVC interops
#pragma pack(push, 1)
    struct DragData {
        const char *fragmentText = nullptr;
        bool isLink = false;
        const char *linkUrl = nullptr;
        const char *linkTitle = nullptr;
        const char *fragmentHtml = nullptr;
        const char *fragmentBaseUrl = nullptr;
        uint64_t bufferSize = 0;
    };
#pragma pack(pop)
}}}}}}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_MSVC_STRUCTURES_DRAGDATA_HPP_
