/* ********************************************************************************************************* *
*
* Copyright (c) 2018 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_MICROCORE_MSVC_DRAGANDDROP_DRAGANDDROP_WINDOWS_HPP
#define COM_WUI_FRAMEWORK_MICROCORE_MSVC_DRAGANDDROP_DRAGANDDROP_WINDOWS_HPP

#include <string>

#include <windows.h>

#include <include/cef_browser.h>
#include <include/cef_drag_data.h>
#include <include/cef_render_handler.h>
#include <include/internal/cef_ptr.h>

#include "MSVC/Callbacks/Callbacks.hpp"
#include "MSVC/Structures/DragData.hpp"

namespace Com { namespace Wui { namespace Framework { namespace Microcore { namespace MSVC { namespace DragAndDrop {
    extern "C" {
        __declspec(dllexport) bool __cdecl Initialize(const char *$id,
                                                      Callbacks::OnDrop $onDrop,
                                                      Callbacks::OnDragLeave $onDragLeave,
                                                      Callbacks::OnDragOver $onDragOver,
                                                      Callbacks::OnDragEnter $onDragEnter,
                                                      HWND $hwnd);

        __declspec(dllexport) void __cdecl Destroy(HWND $hwnd);

        __declspec(dllexport) bool __cdecl HasTarget();

        __declspec(dllexport) CefBrowserHost::DragOperationsMask __cdecl StartDragging(Structures::DragData $dragData,
                                                                                       CefRenderHandler::DragOperationsMask $allowedOps,
                                                                                       int $x,
                                                                                       int $y);
    }
}}}}}}

#endif  // COM_WUI_FRAMEWORK_MICROCORE_MSVC_DRAGANDDROP_DRAGANDDROP_WINDOWS_HPP
