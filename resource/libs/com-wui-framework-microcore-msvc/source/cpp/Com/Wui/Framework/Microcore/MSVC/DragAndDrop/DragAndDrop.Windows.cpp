/* ********************************************************************************************************* *
*
* Copyright (c) 2018 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "MSVC/DragAndDrop/DragAndDrop.Windows.hpp"

#include <mutex>

#include <atlcomcli.h>

#include "MSVC/DragAndDrop/Implementation.Windows.hpp"

namespace Com { namespace Wui { namespace Framework { namespace Microcore { namespace MSVC { namespace DragAndDrop {

std::mutex protection;
CComPtr<DropTargetWin> dropTarget = nullptr;

bool __cdecl Initialize(const char *$id,
                        Callbacks::OnDrop $onDrop,
                        Callbacks::OnDragLeave $onDragLeave,
                        Callbacks::OnDragOver $onDragOver,
                        Callbacks::OnDragEnter $onDragEnter,
                        HWND $hwnd) {
    std::lock_guard<decltype(protection)> lock{ protection };

    LOG(INFO) << "Initialize with id: " << $id << " and HWND: " << $hwnd;

    bool initialized = false;

    if ($onDrop && $onDragLeave && $onDragOver && $onDragEnter && $hwnd) {
        LOG(INFO) << "Gonna register drag-and-drop target";

        dropTarget = DropTargetWin::Create($id, $onDrop, $onDragLeave, $onDragOver, $onDragEnter, $hwnd);
        RegisterDragDrop($hwnd, dropTarget);

        if (dropTarget != nullptr) {
            initialized = true;
        }
        else {
            LOG(ERROR) << "Failed to register drag-and-drop target";
        }
    }

    return initialized;
}

void __cdecl Destroy(HWND $hwnd) {
    std::lock_guard<decltype(protection)> lock{ protection };

    LOG(INFO) << "Destroy for HWND: " << $hwnd;

    // todo: look at the return codes
    RevokeDragDrop($hwnd);
    dropTarget = nullptr;
}

bool __cdecl HasTarget() {
    std::lock_guard<decltype(protection)> lock{ protection };

    return dropTarget != nullptr;
}

CefBrowserHost::DragOperationsMask __cdecl StartDragging(Structures::DragData $dragData,
                                                         CefRenderHandler::DragOperationsMask $allowedOps,
                                                         int $x,
                                                         int $y) {
    std::lock_guard<decltype(protection)> lock{ protection };

    const CefBrowserHost::DragOperationsMask mask = dropTarget->StartDragging($dragData, $allowedOps, $x, $y);

    return mask;
}

}}}}}}
