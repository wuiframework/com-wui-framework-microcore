/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

module.exports = function ($grunt, $cwd, $args, $done) {
    const setUpEnvironment = function () {
        // Preserve just some env variables, others break the MSVC-related build
        const path = process.env.PATH;

        previousEnvironment = process.env;
        process.env = [];

        process.env['PATH'] = path;
    }

    const restorePreviousEnvironment = function () {
        process.env = previousEnvironment;
    }

    const buildTask = function ($buildPath, $targetName, $callback) {
        $grunt.terminalHandler.Spawn(
            'cmake',
            [
                '--build', $buildPath,
                '--target', $targetName,
                '--config', 'Release'
            ],
            {
                cwd: $cwd,
                env: process.env
            },
            function ($exitCode) {
                if ($exitCode === 0) {
                    $callback();
                } else {
                    $grunt.fail.fatal('Build task has failed');
                }
            });
    }

    const installToDependencies = function ($buildPath) {
        buildTask($buildPath, "install", function() {
            restorePreviousEnvironment();

            $done();
        })
    }

    const buildFromSources = function ($buildPath) {
        buildTask($buildPath, "com-wui-framework-microcore-msvc", function() {
            installToDependencies($buildPath);
        })
    }

    const generateFromSources = function ($is64Bit) {
        const sourcePath = '.';
        const buildPath = $grunt.properties.projectBase + '/build/com-wui-framework-microcore-msvc';
        const installPath = $grunt.properties.projectBase + '/dependencies/com-wui-framework-microcore-msvc';
        const cefBinaryPath = $grunt.properties.projectBase + '/dependencies/cef';
        const generator = "Visual Studio 15 2017" + ($is64Bit ? " Win64" : "");
        $cwd = $grunt.properties.projectBase + '/resource/libs/com-wui-framework-microcore-msvc';

        setUpEnvironment();

        $grunt.terminalHandler.Spawn(
            'cmake',
            [
                '-B' + buildPath,
                '-H' + sourcePath,
                '-G', '"' + generator + '"',
                '-DUSE_SANDBOX:BOOL=OFF',
                '-DUSE_ATL:BOOL=ON',
                '-DCMAKE_INSTALL_PREFIX:PATH=' + installPath,
                '-DCEF_BINARY_PATH:PATH=' + cefBinaryPath,
                '-DUSE_CEF_CMAKE_FUNCTIONS:BOOL=ON'
            ],
            {
                cwd: $cwd,
                env: process.env
            },
            function ($exitCode) {
                if ($exitCode === 0) {
                    buildFromSources(buildPath);
                } else {
                    $grunt.fail.fatal('CMake generation has failed');
                }
            });
    };

    if ($args.indexOf("build-from-sources") != -1) {
        previousEnvironment = [];

        generateFromSources(false);
    } else {
        $done();
    }

};
