# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(CEF_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running CEF_CONFIGURE with (\n\tapp_target: ${app_target},\n\ttest_target: ${test_target},\n\tattributes: ${attributes}\n).")
    PARSE_ARGS(attributes)

    set(CEF_ROOT ${CMAKE_SOURCE_DIR}/${path})
    if (IS_ABSOLUTE ${path})
        set(CEF_ROOT ${path})
    endif ()
    set(CEF_PATCH_ROOT ${CMAKE_SOURCE_DIR}/resource/libs/cef-patch)
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CEF_PATCH_ROOT}/cmake)

    set(CEF_INCLUDE_DIRS ${CEF_ROOT} ${CEF_PATCH_ROOT})

    # Execute FindCEF.cmake which must exist in CMAKE_MODULE_PATH whitch is registered from cef-patch folder.
    find_package(CEF REQUIRED)

    # Allow C++ programs to use stdint.h macros specified in the C99 standard that
    # aren't in the C++ standard (e.g. UINT8_MAX, INT64_MIN, etc).
    add_definitions(-D__STDC_CONSTANT_MACROS -D__STDC_FORMAT_MACROS)

    set(CEF_LIBTYPE STATIC)
    set(CEF_COMPILER_FLAGS
            -DUNICODE -fno-strict-aliasing -fstack-protector -funwind-tables -fvisibility=hidden
            --param=ssp-buffer-size=4 -pipe -pthread -Wall -Wno-missing-field-initializers -Wno-unused-parameter
            -Wno-unknown-pragmas
            -include ${CEF_PATCH_ROOT}/include/internal/cef_export.h
            -include ${CEF_PATCH_ROOT}/include/internal/cef_string_wrappers.h
            -include ${CEF_PATCH_ROOT}/include/base/cef_atomicops.h
            -include ${CEF_PATCH_ROOT}/include/base/internal/cef_atomicops_x86_gcc.h)
    set(CEF_CXX_COMPILER_FLAGS
            -fexceptions -fno-threadsafe-statics
            -fvisibility-inlines-hidden -Wsign-compare)
    set(CEF_COMPILER_FLAGS_RELEASE
            -fno-ident -DNDEBUG -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=2)
    set(CEF_LINKER_FLAGS
            -fPIC -pthread -Wl,-rpath,.)

    include(CheckCCompilerFlag)
    include(CheckCXXCompilerFlag)

    CHECK_C_COMPILER_FLAG(-Wno-unused-local-typedefs COMPILER_SUPPORTS_NO_UNUSED_LOCAL_TYPEDEFS)
    if (COMPILER_SUPPORTS_NO_UNUSED_LOCAL_TYPEDEFS)
        set(CEF_C_COMPILER_FLAGS ${CEF_C_COMPILER_FLAGS} -Wno-unused-local-typedefs)
    endif ()

    # -Wno-literal-suffix             = Don't warn about invalid suffixes on literals
    CHECK_CXX_COMPILER_FLAG(-Wno-literal-suffix COMPILER_SUPPORTS_NO_LITERAL_SUFFIX)
    if (COMPILER_SUPPORTS_NO_LITERAL_SUFFIX)
        set(CEF_CXX_COMPILER_FLAGS ${CEF_CXX_COMPILER_FLAGS} -Wno-literal-suffix)
    endif ()

    # -Wno-narrowing                  = Don't warn about type narrowing
    CHECK_CXX_COMPILER_FLAG(-Wno-narrowing COMPILER_SUPPORTS_NO_NARROWING)
    if (COMPILER_SUPPORTS_NO_NARROWING)
        set(CEF_CXX_COMPILER_FLAGS ${CEF_CXX_COMPILER_FLAGS} -Wno-narrowing)
    endif ()

    set(CEF_COMPILER_FLAGS ${CEF_COMPILER_FLAGS} -msse2 -mfpmath=sse -mmmx -m32)
    set(CEF_LINKER_FLAGS ${CEF_LINKER_FLAGS} -m32)

    # Allow the Large File Support (LFS) interface to replace the old interface.
    add_definitions(-D_FILE_OFFSET_BITS=64)

    #
    # Post-configuration actions.
    #
    # Merge compiler/linker flags.
    string(REPLACE ";" " " __flags1 "${CMAKE_C_FLAGS} ${CEF_COMPILER_FLAGS} ${CEF_C_COMPILER_FLAGS}")
    set(CMAKE_C_FLAGS ${__flags1})

    string(REPLACE ";" " " __flags2 "${CMAKE_C_FLAGS_DEBUG} ${CEF_COMPILER_FLAGS_DEBUG} ${CEF_C_COMPILER_FLAGS_DEBUG}")
    set(CMAKE_C_FLAGS_DEBUG ${__flags2})

    string(REPLACE ";" " " __flags3 "${CMAKE_C_FLAGS_RELEASE}${CEF_COMPILER_FLAGS_RELEASE} ${CEF_C_COMPILER_FLAGS_RELEASE}")
    set(CMAKE_C_FLAGS_RELEASE ${__flags3})

    string(REPLACE ";" " " __flags4 "${CMAKE_CXX_FLAGS} ${CEF_COMPILER_FLAGS} ${CEF_CXX_COMPILER_FLAGS}")
    set(CMAKE_CXX_FLAGS ${__flags4})

    string(REPLACE ";" " " __flags5 "${CMAKE_CXX_FLAGS_DEBUG} ${CEF_COMPILER_FLAGS_DEBUG} ${CEF_CXX_COMPILER_FLAGS_DEBUG}")
    set(CMAKE_CXX_FLAGS_DEBUG ${__flags5})

    string(REPLACE ";" " " __flags6 "${CMAKE_CXX_FLAGS_RELEASE} ${CEF_COMPILER_FLAGS_RELEASE} ${CEF_CXX_COMPILER_FLAGS_RELEASE}")
    set(CMAKE_CXX_FLAGS_RELEASE ${__flags6})

    string(REPLACE ";" " " __flags7 "${CMAKE_EXE_LINKER_FLAGS} ${CEF_LINKER_FLAGS} ${CEF_EXE_LINKER_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS ${__flags7})

    string(REPLACE ";" " " __flags8 "${CMAKE_EXE_LINKER_FLAGS_DEBUG} ${CEF_LINKER_FLAGS_DEBUG} ${CEF_EXE_LINKER_FLAGS_DEBUG}")
    set(CMAKE_EXE_LINKER_FLAGS_DEBUG ${__flags8})

    string(REPLACE ";" " " __flags9 "${CMAKE_EXE_LINKER_FLAGS_RELEASE} ${CEF_LINKER_FLAGS} ${CEF_SHARED_LINKER_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE ${__flags9})

    string(REPLACE ";" " " __flags10 "${CMAKE_SHARED_LINKER_FLAGS} ${CEF_LINKER_FLAGS} ${CEF_SHARED_LINKER_FLAGS}")
    set(CMAKE_SHARED_LINKER_FLAGS ${__flags10})

    string(REPLACE ";" " " __flags11 "${CMAKE_SHARED_LINKER_FLAGS_DEBUG} ${CEF_LINKER_FLAGS_DEBUG} ${CEF_SHARED_LINKER_FLAGS_DEBUG}")
    set(CMAKE_SHARED_LINKER_FLAGS_DEBUG ${__flags11})

    string(REPLACE ";" " " __flags12 "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} ${CEF_LINKER_FLAGS_RELEASE} ${CEF_SHARED_LINKER_FLAGS_RELEASE}")
    set(CMAKE_SHARED_LINKER_FLAGS_RELEASE ${__flags12})

    # Include the libcef_dll_wrapper target.
    add_definitions(-DUSING_CEF_SHARED)

    FILE(GLOB_RECURSE CEF_SOURCE_FILES ${CEF_ROOT}/include/*.cc ${CEF_ROOT}/libcef_dll/*.cc)
    FILE(GLOB_RECURSE CEF_HEADER_FILES ${CEF_ROOT}/include/*.h ${CEF_ROOT}/libcef_dll/*.h ${CEF_PATCH_ROOT}/include/*.h)

    add_library(libcef_dll_wrapper ${CEF_SOURCE_FILES} ${CEF_HEADER_FILES})
    set_target_properties(libcef_dll_wrapper PROPERTIES PREFIX "")
    target_include_directories(libcef_dll_wrapper PUBLIC ${CEF_INCLUDE_DIRS})

    # Logical target used to link the libcef library.
    ADD_LOGICAL_TARGET(libcef_lib ${CEF_LIB_DEBUG} ${CEF_LIB_RELEASE})

    COPY_FILES(${app_target} "${CEF_BINARY_FILES}" "${CEF_BINARY_DIR}" "${CMAKE_SOURCE_DIR}/build/target")
    COPY_FILES(${test_target} "${CEF_BINARY_FILES}" "${CEF_BINARY_DIR}" "${CMAKE_SOURCE_DIR}/build/target")
    COPY_FILES(${test_lib_target} "${CEF_BINARY_FILES}" "${CEF_BINARY_DIR}" "${CMAKE_SOURCE_DIR}/build/target")

    # Add wrapper dependency
    target_include_directories(${app_target} PUBLIC ${CEF_INCLUDE_DIRS})
    target_include_directories(${test_target} PUBLIC ${CEF_INCLUDE_DIRS})
    target_include_directories(${test_lib_target} PUBLIC ${CEF_INCLUDE_DIRS})

    target_link_libraries(${app_target}
            libcef_dll_wrapper
            libcef_lib
            ${CEF_STANDARD_LIBS})
    target_link_libraries(${test_target}
            libcef_dll_wrapper
            libcef_lib
            ${CEF_STANDARD_LIBS})
    target_link_libraries(${test_lib_target}
            libcef_dll_wrapper
            libcef_lib
            ${CEF_STANDARD_LIBS})

    add_dependencies(${app_target} libcef_dll_wrapper)
    add_dependencies(${test_target} libcef_dll_wrapper)
    add_dependencies(${test_lib_target} libcef_dll_wrapper)

endmacro()
